
########################################################################################
#############################   VERSION HISTORY  #######################################
#created April 2011
#version 29APR2011
#version 5MAY2011
#version 15JUNE2011
#Version 12JULY1011
#Version 20July2011
#Version 22MAR2013      ##this one to add supplementary data (id available) to the existing dataset
#version 28MAR2013      ## some other fix its for formatting and data issues
#version released 03APR2013
#version 28APR2014
########################################################################################
########################################################################################

date <- '16OCT2014'

#several files are required:
#  main clinical data
# list of Kenyan controls to use
# list of all parent samples according to the Central DB
# list of Kumasi parents

### Load in merged clinical data
setwd("C:/Users/jshelton/Documents/CP1/CP1_remerge_2014/ToPheno_merging/All_country_merges")
clindata <- as.matrix(read.csv("RD_BF_CM_GB_GH_MW_ML_NG_PNG_TZ_VM_KE.csv", header=TRUE, quote="\"", fill=TRUE, na.strings=NA))
clindata.ori <- clindata
clindata.ori <- as.data.frame(clindata.ori)
summary(clindata)
clindata <- as.data.frame(toupper(clindata))

#Kenyan controls to use
kcf <- "extra_files/Kenya_KH_controls_28APR2014.csv"
kencont <- as.matrix(read.delim(kcf, sep=",", header=TRUE, quote="\"", fill=TRUE, na.strings=NA))
kencont <- as.data.frame(toupper(kencont))
names(kencont)
#Parents
# load data file
cpr <- "extra_files/central_parents_25APR2014.csv"
cenpart <- as.matrix(read.delim(cpr, sep=",", header=TRUE, quote="\"", fill=TRUE, na.strings=NA))
cenpart <- as.data.frame(toupper(cenpart))
names(cenpart)

####output file names
clinical.out.csv <- paste("output_files/CP1_clin_phenotypes_ALL_", date, ".csv", sep="")
clinical.out.dict <- paste("output_files/CP1_clin_phenotypes_data_dictionary_", date, ".csv", sep="")

########################################################################################
########################################################################################

## Deal with some simple formatting issues


#drop the 'row' field if present
rws <- which(!names(clindata)=="row")
clindata <- clindata[,rws]
#look see stuff
#data size
nrow(clindata)
names(clindata)
table(clindata$study_code, useNA = "ifany")


# Are there any unexpected study codes? If so, sort.
clindata$study_code <- as.character(clindata$study_code)
clindata$sc_sl      <- as.character(clindata$sc_sl)
# KY should be ZK
clindata[clindata$study_code == "KY",c("study_code", "id", "sc_sl")] # need to change study_code and sc_sl
kyrows <- which(clindata$study_code == "KY")
clindata$study_code[kyrows] <- "ZK"
clindata$sc_sl[kyrows]      <- paste("ZK_",clindata$id[kyrows], sep="")

##which samples have a null study_code?
png <- clindata[which(is.na(clindata$study_code)==T),c("study_code","sc_sl","id")]
dim(png)
png[1:10,]
##PNG because of the study_code NA
#let's just fix this
clindata$study_code[which(substring(clindata$sc_sl,1,3)=='NA_')] <- "NA*"
clindata$study_code <- as.factor(clindata$study_code)
#yippee!!!!!

# check again
table(clindata$study_code, useNA = "ifany")


## convert all \N's to NA
clindata[clindata == '\\N'] <- NA
clindata[clindata == '\\\\N'] <- NA
clindata[clindata == '\\\\\\N'] <- NA
clindata[clindata == '\\\\\\\\N'] <- NA
clindata[clindata == '\\\\\\\\\\N'] <- NA
clindata[clindata == '\\\\\\\\\\\\N'] <- NA
clindata[clindata == 'NULL'] <- NA
clindata[clindata == 'MISSING'] <- NA
clindata[clindata == 'UNKNOWN'] <- NA


#Remove any spaces in sample_label in clinical data files
clindata$sample_label <- gsub(" +","",clindata$sample_label)
#grep(' +',clindata$sample_label)
#change all dashes to underscores in sample_label
clindata$sample_label <- gsub("-+","_",clindata$sample_label)
#change all pluses to underscores in sample_label
clindata$sample_label <- gsub("\\+","_",clindata$sample_label)
##change any single backslahes or forward slashes to underscore
#unfortunately we have to do this by field
for(i in 1:ncol(clindata)){
  #look for forward slashes and replace by underscore
  #convert the field to a character field
  #print(length(grep("/", clindata[,i])))
  clindata[,i] <- as.character(clindata[,i])
  #print(length(grep("/", clindata[,i])))
  clindata[,i] <- gsub("/", "_", clindata[,i])
  #convert back
  clindata[,i] <- as.factor(clindata[,i])
}

#there are 2 known forward slashes in the data
#  in the location_region field
find <- "\\\\\\\n"
clindata[grep(find, clindata$location_region),]
clindata$location_region <- gsub(find, "", clindata$location_region)
#and replace spaces with underscores
clindata$location_region <- gsub(" +", "_", clindata$location_region)
clindata$location_region <- gsub("\\\\", "", clindata$location_region)


#I noticed there is a funny number in location_region that should have been missing
clindata$location_region[clindata$location_region == "888"] <- NA


###check 2 records that have caused problems previously
clindata[which(clindata$parasitemia_parasites_ul == 'ORAL'),]
clindata[which(clindata$id %in% c('1PG241','1PG242')),]


#check for entries missing IDs
missing <- clindata[which(is.na(clindata$id)==T),]
dim(missing)


##date fields have slashes replaced by underscore to allow excel to 'cope'
clindata$admission_date <- gsub('/', '_', clindata$admission_date)
clindata$birth_date <- gsub('/', '_', clindata$birth_date)
clindata$date_death_discharge_absconded <- gsub('/', '_', clindata$date_death_discharge_absconded)
#I noticed there is a funny date in birth_date that should have been missing
clindata$birth_date[clindata$birth_date == "0000_00_00"] <- NA
clindata <- droplevels(clindata)
#check
clindata[1:10,]

##change back-to-front dates in admission_date
addt <- which(substr(clindata$admission_date, 5, 5) == "_")
new_ad_date <- as.character(clindata$admission_date)
new_ad_date[addt] <- paste(substr(clindata$admission_date[addt], 9, 10),substr(clindata$admission_date[addt], 6, 7),substr(clindata$admission_date[addt], 1, 4), sep="_")
clindata$admission_date <- new_ad_date
#check
which(substr(clindata$admission_date, 5, 5) == "_")

##change back-to-front dates in birth_date
bidt <- which(substr(clindata$birth_date, 5, 5) == "_")
new_birth_date <- as.character(clindata$birth_date)
new_birth_date[bidt] <- paste(substr(clindata$birth_date[bidt], 9, 10),substr(clindata$birth_date[bidt], 6, 7),substr(clindata$birth_date[bidt], 1, 4), sep="_")
clindata$birth_date <- new_birth_date
#check
which(substr(clindata$birth_date, 5, 5) == "_")

##change back-to-front dates in birth_date
didt <- which(substr(clindata$date_death_discharge_absconded, 5, 5) == "_")
new_date_death_discharge_absconded <- as.character(clindata$date_death_discharge_absconded)
new_date_death_discharge_absconded[didt] <- paste(substr(clindata$date_death_discharge_absconded[didt], 9, 10),substr(clindata$date_death_discharge_absconded[didt], 6, 7),substr(clindata$date_death_discharge_absconded[didt], 1, 4), sep="_")
clindata$date_death_discharge_absconded <- new_date_death_discharge_absconded
#check
which(substr(clindata$date_death_discharge_absconded, 5, 5) == "_")

names(clindata)

#############################################################################################
###  NOW would be a good time to deal with any duplicates arising
duplicate.sc_sl <- clindata$sc_sl[duplicated(clindata$sc_sl)]
length(duplicate.sc_sl)
duplicates.all <- clindata[which(clindata$sc_sl %in% duplicate.sc_sl),]
dim(duplicates.all)
##what is in the duplicates?
table(duplicates.all$study_code, useNA = "ifany")
#save duplicates
duplicates.out <- paste("output_files/temp_duplicates_after_supp_merger_",date,".txt",sep="")
write.table(duplicates.all, duplicates.out, sep="\t", quote = F, row.names = F, col.names = T)

##write out the resolved duplicates
#duplicates.resolved <- paste("output_files/temp_duplicates_resolved_supp_merger_",date,".txt",sep="")
#write.table(new.df, duplicates.resolved, sep="\t", quote = F, row.names = F, col.names = T)

##what to do with the resolve duplicates?
## remove from clindata and then add the resolved bit back
#clindata <- clindata[which(!clindata$sc_sl %in% duplicate.sc_sl),]
#dim(clindata)
#clindata <- rbind(clindata, new.df)
#dim(clindata)

#######################################################################################################################################

#make all header names lower case for ease of use
names(clindata) <- tolower(names(clindata))
#make sure caseorcontrol is all uppercase
clindata$case_or_control <- toupper(clindata$case_or_control)
table(clindata$case_or_control, useNA="ifany")

#fix a few field names     (add any others here too)
#we need to rename the caseorcontrol field oldcaseorcontrol
#becasue we have to restrict the number of Kenya controls to 4000
#this will be done by loading a file and then creating a new caseorcontrol variable
names(clindata)[which( names(clindata) == "case_or_control" )] <- "oldcaseorcontrol"    #from case_or_control
names(clindata)[which( names(clindata) == "sex" )] <- "clinical_gender"  			#from sex

#When the Topheno data was tidied in R before merging some variable names had the 's' at the end replaced with '_'. The following fixes this
names(clindata)[which( names(clindata) == "estimated_age_month_" )] <- "estimated_age_months"
names(clindata)[which( names(clindata) == "fever_within_past48_hour_" )] <- "fever_within_past48_hours"
names(clindata)[which( names(clindata) == "respiratory_distre_" )] <- "respiratory_distress"
names(clindata)[which( names(clindata) == "admission_date_minus_birth_date_month_" )] <- "admission_date_minus_birth_date_months"
names(clindata)[which( names(clindata) == "generalised_lymphadenopathy_oral_candiasi_" )] <- "generalised_lymphadenopathy_oral_candiasis"
names(clindata)[which( names(clindata) == "capillary_refill_2second_" )] <- "capillary_refill_2seconds"
names(clindata)[which( names(clindata) == "other_diagnosis_gastroenteriti_" )] <- "other_diagnosis_gastroenteritis"
names(clindata)[which( names(clindata) == "other_diagnosis_meningiti_" )] <- "other_diagnosis_meningitis"
names(clindata)[which( names(clindata) == "cerebral_malaria_statu_" )] <- "cerebral_malaria_status"
names(clindata)[which( names(clindata) == "note_" )] <- "notes"

names(clindata)[which( names(clindata) == "malaria__positive" )] <- "malaria_positive"
names(clindata)[which( names(clindata) == "do__not__use" )] <- "do_not_use"
names(clindata)


table(clindata$study_code, clindata$cerebral_malaria_status, useNA="ifany")
table(clindata$study_code, clindata$anaemia_status, useNA="ifany")

# Check for duplicated field names now that some have been reformatted...
variables <- names(clindata)
uniq.var  <- unique(variables) #7 duplicates
dup.var   <- variables[duplicated(variables)]
dup.var

#check
names(clindata)

#############################################################################################

#right let us deal with the sorting out Kenyan Controls for use and reassigning the parents
#create a new variable
clindata$caseorcontrol <- NA
# copy over to caseorcontrol  the oldcaseorcontrol data
clindata$caseorcontrol <- ifelse(is.na(clindata$oldcaseorcontrol)==FALSE, as.character(clindata$oldcaseorcontrol),"OTHER")
#nullify all Kenya controls
table(clindata$caseorcontrol,clindata$study_code)
clindata$caseorcontrol[as.character(clindata$oldcaseorcontrol)=="CONTROL" & as.character(clindata$study_code) == "ZK"] <- NA
#update with kenyan controls
clindata$caseorcontrol[clindata$sc_sl %in% kencont$sc_sl] <- "CONTROL"
table(clindata$caseorcontrol,clindata$study_code, useNA="ifany")


#we also need to sort out all controls from parents or others
#update with parent samples
clindata$caseorcontrol[clindata$sc_sl %in% cenpart$sc_sl] <- "PARENT"
#deal with some danglies!
#GAMBIA
clindata$caseorcontrol[substring(clindata$sc_sl,1,5) == 'GF_DD'  | substring(clindata$sc_sl,1,5) == 'GF_MM'] <- "PARENT"
clindata$caseorcontrol[substring(clindata$sc_sl,1,5) == 'GM_MS'  ] <- "PARENT"
#MALAWI
clindata$caseorcontrol[grep("MF_\\d+[MF]", clindata$sc_sl)] <- "PARENT"
#KENYA
clindata$caseorcontrol[grep("ZK_\\d+[MB]", clindata$sc_sl)] <- "PARENT"
clindata$caseorcontrol[grep("ZK_KF\\d+[-_][MF]", clindata$sc_sl)] <- "PARENT"
clindata$caseorcontrol[grep("ZK_KD\\d+[-_][MB]", clindata$sc_sl)] <- "PARENT"
#GHANA
clindata$caseorcontrol[grep("ZG_[0-9]+[MFmf]", clindata$sc_sl)] <- "PARENT"
clindata$caseorcontrol[grep("ZG_[0-9]+[-_][MFmf]", clindata$sc_sl)] <- "PARENT"

#way to display chunks of data based on a pattern
#the ^ symbol says start at the beginning rather than just look anywhere in the text
#clindata$sc_sl[grep("^GF_", clindata$sc_sl)]

#final catch-all
clindata$caseorcontrol[is.na(clindata$caseorcontrol)==TRUE]  <- "OTHER"

clindata$sc_sl[clindata$study_code == "ZK"]

######################################################################################

#tidy up gender
table(clindata$clinical_gender, useNA='ifany')
#here male=>1 and female=>0
clindata$clinical_gender <- ifelse(clindata$clinical_gender %in% c("MALE","M"),"M",ifelse(clindata$clinical_gender %in% c("FEMALE","F"),"F",NA))
clindata$clinical_gender[clindata$clinical_gender == 'NULL'] <- NA
table(clindata$clinical_gender, useNA='ifany')

clindata$id[which(is.na(clindata$clinical_gender) == T)]

###########################################################
## Recode countries and sites
###########################################################
#(country_code	ccorder	site	site_order
#	SN	1	  Dakar	       1
#	GM	2	  Banjul	     2
#	ML	3	  Bamako	     3
#	BF	4	  Ouagadougou	 4
#	GH	5	  Kumasi	     5
#	GH	5	  Noguchi	     6
#	NG	6	  Ibadan	     7            
#	CM	7	  Buea	       8
#	SD	8	  Khartoum	   9
#	KE	9	  Kilifi	     10
#	TZ	10	Moshi	       11
#	TZ	10	Muhumbili	   12
#	TZ	10	Tanga	       13
#	MW	11	Blantyre	   14
#	TH	12	Bangkok	     15
#	LK	12	Colombo	     16
#	VN	13	Ho_Chi_Minh_City	17
#	PG	14	Madang	     18           
###########################################################


#add numbers to sort countries and sites into a standard order
#create a new variable  and add to data dictionary
clindata$country_code <- NA
clindata$country_name <- NA
clindata$country_order <- NA
clindata$site_name <- NA
clindata$site_order <- NA

#get a list of study_codes
table(clindata$study_code, useNA="ifany")

#look at clindata$study_code
str(clindata$study_code)
#as a factor it cannot be overwritten by characters so need to change to character string
clindata$study_code <- as.character(clindata$study_code)

#the following will cope with all countries we are involved with
#but not necessarily cover all study codes
#the following code will update countries and sites and codes and at the end run a check to see if any samples are outstanding

#this include looking for the new_study_codes: ZB, ZG, ZK
#once we add the original study_codes to the file we just need to process against these.
#so remove the new study_code bits from the code below.
#we use the original study_codes just to assign the countries and sites correctly.

#Senegal
adf <- which( clindata$study_code == "SE" )
clindata$country_code[adf] <- "SE"
clindata$country_name[adf] <- "Senegal"
clindata$country_order[adf] <- 1
clindata$site_name[adf] <- "Dakar"
clindata$site_order[adf] <- 1

#Gambia
adf <- which( clindata$study_code == "GA" | clindata$study_code == "GB" | clindata$study_code == "GF" | clindata$study_code == "GJ" | clindata$study_code == "GM" | clindata$study_code == "GQ" | clindata$study_code == "GU" )
clindata$country_code[adf] <- "GM"
clindata$country_name[adf] <- "Gambia"
clindata$country_order[adf] <- 2
clindata$site_name[adf] <- "Banjul"
clindata$site_order[adf] <- 2

#Mali
adf <- which( clindata$study_code == "ML"  )
clindata$country_code[adf] <- "ML"
clindata$country_name[adf] <- "Mali"
clindata$country_order[adf] <- 3
clindata$site_name[adf] <- "Bamako"
clindata$site_order[adf] <- 3

#Burkina Faso
adf <- which( clindata$study_code == "BD" | clindata$study_code == "BE" )
clindata$country_code[adf] <- "BF"
clindata$country_name[adf] <- "Burkina_Faso"
clindata$country_order[adf] <- 4
clindata$site_name[adf] <- "Ougadougou"
clindata$site_order[adf] <- 4


#############

#Ghana  needs to be split into Kumasi and Noguchi. As old study codes are gone do this by reading in a file with all the Ghana samples and old codes
#Use these to split Ghana into sites
dtn4 <- "extra_files/GHANA_SAMPLES_28APR2014.csv"

ghana.samples=read.csv(dtn4, sep=",", header=TRUE, quote="\"")
names(ghana.samples)
dim(ghana.samples)

kumsam <- ghana.samples[ghana.samples$study_code=="CK" | ghana.samples$study_code == "CP" | ghana.samples$study_code == "GN",]
nogsam <- ghana.samples[ghana.samples$study_code=="CC" | ghana.samples$study_code == "CD" | ghana.samples$study_code == "CN",]

table(ghana.samples$study_code, useNA = "ifany")




#look for Ghana study codes
bob <- which(substring(clindata$sc_sl, 1 ,3) %in% c("CK_", "CP_", "GN_") | clindata$study_code %in% c("CK", "CP", "GN"))
clindata$study_code[bob] <- "ZG"
clindata$sc_sl <- as.character(clindata$sc_sl)
clindata$sc_sl[bob] <- paste("ZG_",substring(clindata$sc_sl[bob],4,30),sep="")
clindata$site_name[bob] <- 'Kumasi'
clindata$site_order[bob] <- 5

mia <- which(substring(clindata$sc_sl, 1 ,3) %in% c("CC_", "CD_", "CN_") | clindata$study_code %in% c("CC", "CD", "CN"))
clindata$study_code[mia] <- "ZG"
clindata$sc_sl[mia] <- paste("ZG_",substring(clindata$sc_sl[mia],4,30),sep="")
clindata$site_name[mia] <- 'Noguchi'
clindata$site_order[mia] <- 6

table(clindata$study_code, useNA="ifany")
#clindata$study_code <- droplevels(clindata$study_code)
table(substring(clindata$sc_sl, 1 ,2), useNA="ifany")

#clindata[clindata$study_code %in% c("NO", "YE"),]
#clindata[substring(clindata$sc_sl, 1 ,2) %in% c("NO", "YE"),]

clindata$site_name[clindata$sc_sl %in% kumsam$sc_sl] <- 'Kumasi'
clindata$site_order[clindata$sc_sl %in% kumsam$sc_sl] <- 5

clindata$site_name[clindata$sc_sl %in% nogsam$sc_sl] <- 'Noguchi'
clindata$site_order[clindata$sc_sl %in% nogsam$sc_sl] <- 6

table(clindata$site_name, useNA="ifany")
table(clindata$site_order, useNA="ifany")

##write.table(clindata$sc_sl[which(clindata$study_code == 'ZG')], paste(fl, "Clinical_files/ploppy.txt", sep=""), sep="\t", row.names = F, col.names = T, quote=F)

######################################################################################
#Make sure there are no records without a study code for ZG
var <- which(clindata$study_code == "ZG" & is.na(clindata$site_order) == TRUE)
fields <- c("sc_sl","study_code","country_code","country_name", "country_order", "site_name", "site_order")
x <- as.data.frame(clindata[var,fields])
dim(x)


##we can tidy-up some of these

clindata$site_name[intersect(grep("ZG_\\d\\d\\d\\d", clindata$sc_sl), which(is.na(clindata$site_order) == T))] <- 'Kumasi'
clindata$site_order[intersect(grep("ZG_\\d\\d\\d\\d", clindata$sc_sl), which(is.na(clindata$site_order) == T))] <- 5
clindata$site_name[intersect(grep("ZG_CO", clindata$sc_sl), which(is.na(clindata$site_order) == T))] <- 'Kumasi'
clindata$site_order[intersect(grep("ZG_CO", clindata$sc_sl), which(is.na(clindata$site_order) == T))] <- 5


clindata$site_name[intersect(grep("ZG_CCS", clindata$sc_sl), which(is.na(clindata$site_order) == T))] <- 'Noguchi'
clindata$site_order[intersect(grep("ZG_CCS", clindata$sc_sl), which(is.na(clindata$site_order) == T))] <- 6

clindata[which(clindata$study_code == 'ZG' ),fields]

# check again
var <- which(clindata$study_code == "ZG" & is.na(clindata$site_order) == TRUE)
var

######################################################################################

#Ghana
adf <- which( clindata$study_code == "ZG") 
clindata$country_code[adf] <- "ZG"
clindata$country_name[adf] <- "Ghana"
clindata$country_order[adf] <- 5


#Nigeria
adf <- which(clindata$study_code == "NS" | clindata$study_code == "NT" | clindata$study_code == "NR" )
clindata$country_code[adf] <- "NG"
clindata$country_name[adf] <- "Nigeria"
clindata$country_order[adf] <- 6
clindata$site_name[adf] <- "Ibadan"
clindata$site_order[adf] <- 7

#Cameroon (include new study_code)
adf <- which( clindata$study_code == "ZB" | clindata$study_code == "BM" | clindata$study_code == "BN" )
clindata$country_code[adf] <- "CM"
clindata$country_name[adf] <- "Cameroon"
clindata$country_order[adf] <- 7
clindata$site_name[adf] <- "Buea"
clindata$site_order[adf] <- 8

#Sudan
adf <- which( clindata$study_code == "SA" | clindata$study_code == "SF"  | clindata$study_code == "SD")
clindata$country_code[adf] <- "SD"
clindata$country_name[adf] <- "Sudan"
clindata$country_order[adf] <- 8
clindata$site_name[adf] <- "Khartoum"
clindata$site_order[adf] <- 9
 
#Kenya  (include new study_code)
adf <- which(clindata$study_code == "ZK" |  clindata$study_code == "KA" | clindata$study_code == "KB" | clindata$study_code == "KC" | clindata$study_code == "KD" | clindata$study_code == "KE" | clindata$study_code == "KF" | clindata$study_code == "KJ" | clindata$study_code == "KK" | clindata$study_code == "KL" | clindata$study_code == "KN" | clindata$study_code == "KH" | clindata$study_code == "KP" )
clindata$country_code[adf] <- "KE"
clindata$country_name[adf] <- "Kenya"
clindata$country_order[adf] <- 9
clindata$site_name[adf] <- "Kilifi"
clindata$site_order[adf] <- 10

#Tanzania
adf <- which( clindata$study_code == "TJ" | clindata$study_code == "TK" )
clindata$country_code[adf] <- "TZ"
clindata$country_name[adf] <- "Tanzania"
clindata$country_order[adf] <- 10
clindata$site_name[adf] <- "Moshi"
clindata$site_order[adf] <- 11

#Tanzania
adf <- which( clindata$study_code == "TG" | clindata$study_code == "TP" )
clindata$country_code[adf] <- "TZ"
clindata$country_name[adf] <- "Tanzania"
clindata$country_order[adf] <- 10
clindata$site_name[adf] <- "Tanga"
clindata$site_order[adf] <- 13

#Tanzania
adf <- which( clindata$study_code == "TE")
clindata$country_code[adf] <- "TZ"
clindata$country_name[adf] <- "Tanzania"
clindata$country_order[adf] <- 10
clindata$site_name[adf] <- "Muhumbili"
clindata$site_order[adf] <- 12

#Malawi
adf <- which( clindata$study_code == "MF" | clindata$study_code == "MU" )
clindata$country_code[adf] <- "MW"
clindata$country_name[adf] <- "Malawi"
clindata$country_order[adf] <- 11
clindata$site_name[adf] <- "Blantyre"
clindata$site_order[adf] <- 14

#Sri Lanka
adf <- which( clindata$study_code == "SL" )
clindata$country_code[adf] <- "LK"
clindata$country_name[adf] <- "Sri_Lanka"
clindata$country_order[adf] <- 12
clindata$site_name[adf] <- "Colombo"
clindata$site_order[adf] <- 15

#Thailand
adf <- which( clindata$study_code == "TL" )
clindata$country_code[adf] <- "TL"
clindata$country_name[adf] <- "Thailand"
clindata$country_order[adf] <- 13
clindata$site_name[adf] <- "Bangkok"
clindata$site_order[adf] <- 16

#Vietnam
adf <- which( clindata$study_code == "VA" | clindata$study_code == "VB" | clindata$study_code == "VC" | clindata$study_code == "VF" | clindata$study_code == "VM" | clindata$study_code == "VU")
clindata$country_code[adf] <- "VN"
clindata$country_name[adf] <- "Viet_Nam"
clindata$country_order[adf] <- 14
clindata$site_name[adf] <- "Ho_Chi_Minh_City"
clindata$site_order[adf] <- 17

#Papua New Guinea
adf <- which( clindata$study_code == "NA*" | clindata$study_code == "NB" | clindata$study_code == "NC" | clindata$study_code == "ND" | clindata$study_code == "NG" | clindata$study_code == "NH" | clindata$study_code == "NJ")
clindata$country_code[adf] <- "PG"
clindata$country_name[adf] <- "Papua_New_Guinea"
clindata$country_order[adf] <- 15
clindata$site_name[adf] <- "Madang"
clindata$site_order[adf] <- 18


table(clindata$site_order, clindata$study_code, useNA="ifany")

#check  for any missed records
table(clindata$country_code,clindata$country_name, useNA="ifany")
table(clindata$country_order,clindata$country_name, useNA="ifany")
table(clindata$country_code, useNA="ifany")
blankcells <- sum(is.na(clindata$country_code))
totalrows <-  nrow(clindata)
if(is.numeric(blankcells) > 0){
   nbc <- paste("No. blank cells: ",blankcells, "\n", sep="")
   pbc <- paste("% blank cells: ",(blankcells/totalrows)*100, "\n", sep="")
   lev <- paste(levels(clindata$study_code[is.na(clindata$country_code)]), collapse=" ")
   print(paste(nbc,pbc,lev, "\n", sep="            "))
   #display rows
   empt <- which( is.na(clindata$country_code) )
   table(clindata$study_code[empt])
   clindata[empt,fields]
   stop("CODE EXECUTION STOPPED")
}


##tidy some country names
#BF
#VN
#PG
clindata$country_name <- gsub("Viet_Nam", "VietNam",clindata$country_name)
clindata$country_name <- gsub("Burkina_Faso", "BurkinaFaso",clindata$country_name)
clindata$country_name <- gsub("Papua_New_Guinea", "PapuaNewGuinea",clindata$country_name)


###Tidy some study codes and sample_labels to match those in the genotyped samples file
table(clindata$study_code)
clindata$study_code[clindata$study_code == "BM" | clindata$study_code == "BN"] <- "ZB"
clindata$study_code[clindata$study_code == "KD" | clindata$study_code == "KH"] <- "ZK"
clindata$study_code[clindata$study_code == "ML"]                           <- "DG"
#fingers crossed there are now the same study codes!
table(clindata$study_code)

###Gaps in several sample_labels are also causing problems
clindata$sample_label <- gsub(" ","", clindata$sample_label)

###############################################################################################

#Now want to put the site as factors so that the levels are correct
#convert to factors and order the levels
clindata$site_name <- factor(clindata$site_name, levels= c("Banjul", "Bamako", "Ougadougou", "Kumasi", "Noguchi", "Ibadan", "Buea", "Kilifi", "Moshi", "Blantyre",  "Ho_Chi_Minh_City", "Madang")) 

#Creat a new variable that names the countries and splits Ghana by site
clindata$country_site <- clindata$site_order
table(clindata$country_site, useNA="ifany")
clindata$country_site[clindata$site_order==2] <- "Gambia"
clindata$country_site[clindata$site_order==3] <- "Mali"
clindata$country_site[clindata$site_order==4] <- "BurkinaFaso"
clindata$country_site[clindata$site_order==5] <- "Ghana-Kumasi"
clindata$country_site[clindata$site_order==6] <- "Ghana-Noguchi"
clindata$country_site[clindata$site_order==7] <- "Nigeria"
clindata$country_site[clindata$site_order==8] <- "Cameroon"
clindata$country_site[clindata$site_order==10] <- "Kenya"
clindata$country_site[clindata$site_order==11] <- "Tanzania"
clindata$country_site[clindata$site_order==14] <- "Malawi"
clindata$country_site[clindata$site_order==17] <- "Vietnam"
clindata$country_site[clindata$site_order==18] <- "PapuaNewGuinea"


clindata$country_site <- factor(clindata$country_site, levels= c("Gambia", "Mali", "BurkinaFaso", "Ghana-Kumasi", "Ghana-Noguchi", "Nigeria", "Cameroon", "Kenya", "Tanzania", "Malawi", "Vietnam", "PapuaNewGuinea")) 

table(clindata$country_site, useNA="ifany")


###############################################################################################

#sort ethnicities and create a curated_ethnicity varable
#tidy-up ethnicities
names(clindata)  
table(clindata$ethnic_group_mother, useNA="ifany")
table(clindata$ethnic_group_father, useNA="ifany")
#look for any odd ethnic groups and reformat as necessary
# we use gsub to globally replace if there exists more than one instance of pattern in the text
#both_parents_madang or both_parents_sepik
clindata$ethnic_group_mother <- gsub('BOTH PARENTS MADANG','MADANG',clindata$ethnic_group_mother)
clindata$ethnic_group_father <- gsub('BOTH PARENTS MADANG','MADANG',clindata$ethnic_group_father)
clindata$ethnic_group_mother <- gsub('BOTH PARENTS SEPIK','SEPIK',clindata$ethnic_group_mother)
clindata$ethnic_group_father <- gsub('BOTH PARENTS SEPIK','SEPIK',clindata$ethnic_group_father)


#first a bit of curation to remove any 'offending' characters
#backslash to underscore
clindata$ethnic_group_mother <- gsub("/","_",clindata$ethnic_group_mother)
clindata$ethnic_group_father <- gsub("/","_",clindata$ethnic_group_father)
#update current ethnicities so that all spaces are replaced by underscores
clindata$ethnic_group_mother <- gsub(" +", "_",clindata$ethnic_group_mother)
clindata$ethnic_group_father <- gsub(" +", "_",clindata$ethnic_group_father)
#remove apostrophes
clindata$ethnic_group_mother <- gsub("\'+", "",clindata$ethnic_group_mother)
clindata$ethnic_group_father <- gsub("\'+", "",clindata$ethnic_group_father)
#remove commas
clindata$ethnic_group_mother <- gsub(",+", "_and_",clindata$ethnic_group_mother)
clindata$ethnic_group_father <- gsub(",+", "_and_",clindata$ethnic_group_father)

#finally tidy up any duplicated undescores
clindata$ethnic_group_mother <- gsub("_+", "_",clindata$ethnic_group_mother)
clindata$ethnic_group_father <- gsub("_+", "_",clindata$ethnic_group_father)


#there is something funny with the ethnic group S_tieng
#  in the ethnic group mother/father field
stieng <- unique(c(grep("S_TIENG",clindata$ethnic_group_mother), grep("S_TIENG",clindata$ethnic_group_father)))
table(clindata[stieng,"ethnic_group_mother"])
table(clindata[stieng,"ethnic_group_father"])
clindata$ethnic_group_mother <- gsub("^\\s+|\\s+$", "", clindata$ethnic_group_mother)
clindata$ethnic_group_father <- gsub("^\\s+|\\s+$", "", clindata$ethnic_group_father)
#check again
table(clindata[stieng,"ethnic_group_mother"])
table(clindata[stieng,"ethnic_group_father"])

##################################################
# Add in ethnicities for the parents of Kumasi trios

#Data table name from stub 
ghana.m <- "extra_files/Ghana_Kumasi_TDT_trios_ethnicities_MOTHERS_28APR2014.csv"
ghana.f <- "extra_files/Ghana_Kumasi_TDT_trios_ethnicities_FATHERS_28APR2014.csv"

#load data for mothers and fathers
trios.m=read.csv(ghana.m, sep=",", header=TRUE, quote="\"", fill=TRUE, na.strings=NA)
trios.f=read.csv(ghana.f, sep=",", header=TRUE, quote="\"", fill=TRUE, na.strings=NA)
names(trios.m)
names(trios.f)
table(trios.m$ethnicity_translated, trios.f$ethnicity_translated, exclude=NULL)

table(clindata$ethnic_group_mother[clindata$site_name == "Kumasi"], exclude=NULL)
table(clindata$ethnic_group_father[clindata$site_name == "Kumasi"], exclude=NULL)
table(trios.f$ethnicity_translated)
table(trios.m$ethnicity_translated)

#Match on the sc_sl column in both files and replace ethnicities in clindata file with those in hte ghana trios file NB needed to use as.character to run it correctly

  these.entries <- which(clindata$sc_sl %in% trios.m$sc_sl)
  origi.entries <- which(trios.m$sc_sl  %in% clindata$sc_sl[these.entries])

   clindata$ethnic_group_mother[these.entries]  <-  as.character(trios.m$ethnicity_translated[origi.entries]);
   
these.entries.f <- which(clindata$sc_sl %in% trios.f$sc_sl)
origi.entries.f <- which(trios.f$sc_sl  %in% clindata$sc_sl[these.entries.f])

clindata$ethnic_group_father[these.entries.f]  <-  as.character(trios.f$ethnicity_translated[origi.entries.f]);

table(clindata$ethnic_group_mother[clindata$site_name == "Kumasi"], useNA="ifany")
table(clindata$ethnic_group_father[clindata$site_name == "Kumasi"], useNA="ifany")

##################################################
#check
clindata <- droplevels(clindata)
 table(as.character(clindata$ethnic_group_father),(as.character(clindata$ethnic_group_mother)), useNA="ifany")

#this part was quite challenging as I found it hard to compare the two columns
#the same problem may occur in other variable creations
#e.g. replace spaces with underscores, slashes with underscores

#one solution as shown below
# create out new variable column (temp that can be re-sed for other recodings)
clindata$temp <- NA
           
#update current ethnic_group_mother and ethnic_group_father by setting any null values to "NOT_RECORDED"
#clindata$ethnic_group_mother[clindata$ethnic_group_mother=='NULL']<-"NOT_RECORDED"
#clindata$ethnic_group_father[clindata$ethnic_group_father=='NULL']<-"NOT_RECORDED"
#clindata$ethnic_group_mother[is.na(clindata$ethnic_group_mother)==TRUE]<-"NOT_RECORDED"
#clindata$ethnic_group_father[is.na(clindata$ethnic_group_father)==TRUE]<-"NOT_RECORDED"

#now all records have a ethnic coding so we can simply compare
#plan and test order
#find all ethnic_group_mother and ethnic_group_father that are equal (this also deals with the double NOT_RECORDED)  => code as 1
#find all records where ethnic_group_mother is NOT_RECORDED and ethnic_group_father is not NOT_RECORDED              => code as 2
#find all records where ethnic_group_father is NOT_RECORDED and ethnic_group_mother is not NOT_RECORDED              => code as 3
#so we should be left with only records that are NOT    NOT_RECORDED   and that are unequal      => code as 4
#stick output into a temporary field that we can re-use
clindata$temp <- ifelse(
    #mum and dad are valid
    is.na(clindata$ethnic_group_mother) == "FALSE" & is.na(clindata$ethnic_group_father) == "FALSE", 1, ifelse(
    #mum is valid and dad is not valid 
      is.na(clindata$ethnic_group_mother) == "FALSE" & is.na(clindata$ethnic_group_father) == "TRUE", 2, ifelse(
        # dad is valid and mum is not valid
        is.na(clindata$ethnic_group_mother) == "TRUE" & is.na(clindata$ethnic_group_father) == "FALSE", 3,
          # everything else NA
          9)))


clindata$temp[clindata$temp == 1 & clindata$ethnic_group_mother != "NOT_RECORDED" & clindata$ethnic_group_father == "NOT_RECORDED"] <- 2
clindata$temp[clindata$temp == 1 & clindata$ethnic_group_mother == "NOT_RECORDED" & clindata$ethnic_group_father != "NOT_RECORDED"] <- 3
clindata$temp[clindata$temp == 1 & (clindata$ethnic_group_mother != clindata$ethnic_group_father)] <- 4


#catch_all - look for any zeroes that may be left in the curated_ethnicity field
#look first
table(clindata$temp, useNA="ifany")
table(clindata$ethnic_group_mother, clindata$temp,  useNA="ifany")
table(clindata$ethnic_group_father, clindata$temp,  useNA="ifany")

length(which(is.na(clindata$ethnic_group_mother)))
length(which(is.na(clindata$ethnic_group_father)))
length(which(is.na(clindata$temp)))
length(which(clindata$temp == "4"))

#NOW we can recode all the numbers for the appropriate ethnic groups (couldn't get this work in one step!)
#there must be an easier way to do this but this method does work
#now convert curated_etnicty to appropriate values using a sequential set of logical tests
#plan:
clindata$curated_ethnicity <- NA
# replace 1 or 3 with ethnic_group_mother
# replace 2 with ethnic_group_father
# replace 4 with ethnic_group_mother + "_MIXED"
clindata$curated_ethnicity <- 
ifelse(as.numeric(as.character(clindata$temp)) == 1 | as.numeric(as.character(clindata$temp)) == 2,
   as.character(clindata$ethnic_group_mother),                                      #if test is 1 or 2 then recode as ethnic_group_mother
   ifelse(as.numeric(as.character(clindata$temp)) == 3,                                             #if test is not 1 then retest for 2
      as.character(clindata$ethnic_group_father),                                   #if test is 3 then recode as ethnic_group_father
      ifelse(as.numeric(as.character(clindata$temp)) == 4,                                          #if test is not 2 then retest for 4
         as.character(paste(clindata$ethnic_group_mother,"_MIXED",sep="")),   #if test is 4 then recode as ethnic_group_mother + "_MIXED"
         NA                                                               #if test is 9 then recode as NA
      )
   )
)


#look at result
table(clindata$curated_ethnicity, clindata$temp, useNA="ifany")
#there should not be any empty records from the way the above code works as anything left is always coded
#clear the temp field
#look at result
#clindata$temp <- NULL
#now adjust a few of the curated_ethnicicties


clindata$curated_ethnicity <- gsub('1_PARENT_MADANG_1_PARENT_OTHER','MADANG_MIXED', clindata$curated_ethnicity)
clindata$curated_ethnicity <- gsub('1_PARENT_MADANG_1_PARENT_MOROBE','MADANG_MIXED',clindata$curated_ethnicity)
clindata$curated_ethnicity <- gsub('1_PARENT_MADANG_1_PARENT_SEPIK','MADANG_MIXED', clindata$curated_ethnicity)
clindata$curated_ethnicity <- gsub('1_PARENT_MOROBE_1_PARENT_SEPIK','SEPIK_MIXED',  clindata$curated_ethnicity)
clindata$curated_ethnicity <- gsub('1_PARENT_SEPIK_1_PARENT_OTHER','SEPIK_MIXED',   clindata$curated_ethnicity)

table(clindata$curated_ethnicity, clindata$temp, useNA="ifany")

# Remove "temp" now that ethnicity has been curated
clindata <- clindata[-which(names(clindata) == "temp")]

#################################################################
##adjust Malawi
## make all ethnicities Malawi as there is no division between ethnic groups in this country
addmargins(table(clindata$curated_ethnicity[clindata$country_name %in% c("MALAWI", "Malawi", "malawi", "MALAWI_MALAWI")], useNA="ifany"))
table(clindata$curated_ethnicity, useNA="ifany")
bingo <- which(clindata$country_name %in% c("MALAWI", "Malawi", "malawi"))
clindata$curated_ethnicity[bingo] <- "MALAWI"
#mval <- unique(clindata$ce_sort_order[clindata$country_ethnicity == "MALAWI_MALAWI"])
#clindata$ce_sort_order[bingo] <- as.numeric(mval)
addmargins(table(clindata$curated_ethnicity[clindata$country_name %in% c("MALAWI", "Malawi", "malawi", "MALAWI_MALAWI")], useNA="ifany"))

#---------------------------------------------------
#country_ethnicity
#----------------------------

clindata$country_ethnicity <- paste(clindata$country_site, clindata$curated_ethnicity, sep="_")

table(clindata$country_ethnicity, useNA="ifany")

#------------------------------------------------------------
#Calc frequencies of ethnicities within each country 


output <- data.frame(matrix(, nc=6))
colnames(output) <- c("site_order", "country_site", "country_ethnicity", "proportion","new_country_ethnicity", "ce_sort_order")

group.names <- unique(clindata$site_order) 
n.group <- length(group.names)
counter = 0
for (i in 1:n.group) {
   a <- which(clindata$site_order == group.names[i] & is.na(clindata$curated_ethnicity) ==FALSE)
   b <- as.data.frame(table(clindata$country_ethnicity[a])/length(a) )
   cn <- unique(clindata$country_site[clindata$site_order == group.names[i]])
   	 for( j in 1:nrow(b)){
     	 	counter = counter + 1
     	 	output[counter ,1] <- group.names[i]
      	output[counter ,2] <- as.character(cn)
      	output[counter ,3] <- as.character(b[j,1])
      	output[counter ,4] <- b[j,2]    
   	}
}

#now convert names according to proportions
# <0.05 cutoff
output$new_country_ethnicity <- ifelse(as.numeric(as.character(output$proportion)) >= 0.05, output$country_ethnicity, paste(output$country_site, "OTHER", sep="_"))
#update temp_CE
output$new_country_ethnicity <- gsub('_OTHER','_ZOTHER', output$new_country_ethnicity)


#now order new_country_ethnicity
output <- output[order(output$site_order, output$new_country_ethnicity),]
st <- 0
vv <- ""
for(i in 1: nrow(output)){
   if(as.character(output[i,5]) == vv){
      output[i,6] = st
   }else{
      st = st + 1
      output[i,6] = st
      vv = as.character(output[i,5])
   }  
}
#revert new codes
output$new_country_ethnicity <- gsub('_ZOTHER','_OTHER', output$new_country_ethnicity)
output


#match back on country_ethnicity and put back ce_sort_order
clindata$ce_sort_order <- NA
clindata$new_country_ethnicity <- NA
for ( i in 1:length(output$country_ethnicity)){
   these.entries = which(clindata$country_ethnicity %in% output$country_ethnicity[i])
   #ce_sort_order[these.entries]          = output$ce_sort_order[i];
   #new_country_ethnicity[these.entries]  = output$new_country_ethnicity[i];
   clindata$ce_sort_order[these.entries]          = output$ce_sort_order[i];
   clindata$new_country_ethnicity[these.entries]  = output$new_country_ethnicity[i];
}
#add to clindata

#replace nulls with either 999 or the not_recorded
#clindata$ce_sort_order[is.na(clindata$ce_sort_order)==TRUE] <-999

#clindata$new_country_ethnicity[is.na(clindata$new_country_ethnicity)==TRUE] <- 999
#bingo <- which(clindata$new_country_ethnicity == 999)
#clindata$new_country_ethnicity[bingo] <-as.character(clindata$country_ethnicity[bingo])

unique(clindata$new_country_ethnicity)
table(clindata$new_country_ethnicity, useNA="ifany")


########################################################################################################################
###############################################################################################
###############################################################################################

#clean-up any last bits
## Deal with some simple formatting issues
## convert all \N's to NA
clindata[clindata == '\\N'] <- NA
clindata[clindata == '\\\\N'] <- NA
clindata[clindata == '\\\\\\N'] <- NA
clindata[clindata == '\\\\\\\\N'] <- NA
clindata[clindata == '\\\\\\\\\\N'] <- NA
clindata[clindata == '\\\\\\\\\\\\N'] <- NA
clindata[clindata == 'NULL'] <- NA
clindata[clindata == 'MISSING'] <- NA
clindata[clindata == 'UNKNOWN'] <- NA


#Remove any spaces in sample_label in genotype and clinical data files
clindata$sample_label <- gsub(" +","",clindata$sample_label)
#grep(' +',clindata$sample_label)
#change all dashes to underscores in sample_label
clindata$sample_label <- gsub("-+","_",clindata$sample_label)
#change all pluses to underscores in sample_label
clindata$sample_label <- gsub("\\+","_",clindata$sample_label)
##change any single backslahes or forward slashes to underscore
#unfortunately we have to do this by field
for(i in 1:ncol(clindata)){
  #look for forward slashes and replace by underscore
  #convert the field to a character field
  #print(length(grep("/", clindata[,i])))
  clindata[,i] <- as.character(clindata[,i])
  #print(length(grep("/", clindata[,i])))
  clindata[,i] <- gsub("/", "_", clindata[,i])
  #convert back
  clindata[,i] <- as.factor(clindata[,i])
}
clindata$sample_label <- as.character(clindata$sample_label)
clindata$sample_label <- toupper(clindata$sample_label)
clindata$sample_label <- as.factor(clindata$sample_label)


##date fields have slashes replaced by underscore to allow excel to 'cope'
clindata$admission_date <- gsub('/', '_', clindata$admission_date)
clindata$birth_date <- gsub('/', '_', clindata$birth_date)
clindata$date_death_discharge_absconded <- gsub('/', '_', clindata$date_death_discharge_absconded)


clindata <- droplevels(clindata)

table(clindata$study_code, useNA = "ifany")


#check
clindata[1:10,]

## Funny character in a Malian location causes the entry to be split over two rows. Sort this out:
clindata$location_village[clindata$id == "1TS423"] <- "KATIBOUGOU"
#check
clindata$location_village[clindata$id == "1TS423"]

###################################################################################################

#--------------------------------------------------------------------------------------------------------------------
#**age in months
#------------------------------------------------------------------------------------------------------------------------

#re-calculate ad_date_minus_birth_date now that dates have been reformatted
ad_date <- as.Date(clindata$admission_date, format = "%d_%m_%Y")
bi_date <- as.Date(clindata$birth_date, format = "%d_%m_%Y")
addt_minus_bidt <- difftime(ad_date, bi_date, units="days")
addt_minus_bidt <- round((addt_minus_bidt)/30,0)

#new ad_date_minus_birth_date contains more information than the original; therefore use instead
length(which(!is.na(addt_minus_bidt)))
length(which(!is.na(clindata$admission_date_minus_birth_date_months)))
#combine the two fields
addt_minus_bidt <- as.character(addt_minus_bidt)
clindata$admission_date_minus_birth_date_months <- addt_minus_bidt
#check
length(which(!is.na(clindata$admission_date_minus_birth_date_months)))

#remove underscores from these cols
clindata$admission_date_minus_birth_date_months <- gsub("_","",clindata$admission_date_minus_birth_date_months)
table(clindata$admission_date_minus_birth_date_months)

clindata$estimated_age_months <- gsub("_","",clindata$estimated_age_months)
table(clindata$estimated_age_months)


#create a new variable
clindata$age_months <- NA

#sort out negative ages -> make positive (birth data admin date wrong way around?)
#identify the rows
hp <- which(sign(as.numeric(clindata$admission_date_minus_birth_date_months))== -1)
# multiply data by -1 to change the sign to positive
clindata[hp,"admission_date_minus_birth_date_months"] <- as.numeric(clindata[hp,"admission_date_minus_birth_date_months"])* -1
#deal with NULL in column
clindata$age_months <- clindata$estimated_age_months
agm <- which(is.na(clindata$estimated_age_months) ==TRUE & is.na(clindata$admission_date_minus_birth_date_months) == FALSE)
clindata$age_months[agm] <- clindata$admission_date_minus_birth_date_months[agm]
length(which(!is.na(clindata$estimated_age_months)))
length(which(!is.na(clindata$age_months)))
table(clindata$age_months)
table(clindata$admission_date_minus_birth_date_months)

#----------------------------------------------------------------------------------------------------------------------
#**admission_date
#------------------------------------------------------------------------------------------------------------------------

str(clindata$admission_date)

# admission year
clindata$admission_year  <- substr(clindata$admission_date, 7, 10)
table(clindata$admission_year)

# admission month
clindata$admission_month <- substr(clindata$admission_date, 4, 5)
clindata$admission_month <- factor(clindata$admission_month, levels = c("01","02","03","04","05","06","07","08","09","10","11","12"), 
                        labels = c("January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"))
table(clindata$admission_month)

# concatenate admission month & year
clindata$admission_month_year <- NA
clindata$admission_month_year[!is.na(clindata$admission_month)] <- paste(clindata$admission_month[!is.na(clindata$admission_month)], clindata$admission_year[!is.na(clindata$admission_month)], sep = "_")
table(clindata$admission_month_year)

#----------------------------------------------------------------------------------------------------------------------
#**age_groups
#------------------------------------------------------------------------------------------------------------------------
#define age groups
library(car)
table(clindata$age_months, useNA="ifany")
#fix some inconsistencies
clindata$age_months[which(clindata$age_months == '#VALUE!')] <- NA
clindata$age_months[grep("-   -",clindata$age_months)] <- NA
table(clindata$age_months, useNA="ifany")

# Define age group with 5 groupings
clindata$agegp5 <- recode(as.numeric(as.character(clindata$age_months)), "0:12='0'; 12:24='1'; ; 25:60='2'; 60:180='3'; 181:1000='4'")
clindata$agegp5<-factor(clindata$agegp5)
levels(clindata$agegp5)<-c("<1","1-2","2-5","5-15", ">15")
table(clindata$agegp5, useNA="ifany")

# Define age group with 6 groupings
clindata$agegp6 <- recode(as.numeric(as.character(clindata$age_months)), "0:12='0'; 12:24='1'; ; 25:60='2'; 60:108='3'; 109:180='4'; 181:1000='5'")
clindata$agegp6<-factor(clindata$agegp6)
levels(clindata$agegp6)<-c("<1","1-2","2-5","5-9", "10-15", ">15")
table(clindata$agegp6, useNA="ifany")

#------------------------------------------------------------------------------------------
#***Parasitaemia
#------------------------------------------------------------------------------------------

# Falciparum parasitemia field (combined parasitemia and falciparum parasitemia fields)
clindata$curated_falciparum_parasitemia <- as.character(clindata$parasitemia_parasites_ul)
clindata$curated_falciparum_parasitemia[is.na(clindata$parasitemia_parasites_ul) & !is.na(clindata$falciparum_parasitemia_parasites_�l)] <- clindata$falciparum_parasitemia_parasites_�l[is.na(clindata$parasitemia_parasites_ul) & !is.na(clindata$falciparum_parasitemia_parasites_�l)]
length(which(!is.na(clindata$parasitemia_parasites_ul))); length(which(!is.na(clindata$curated_falciparum_parasitemia)))


# Falciparum parasitemia present/absent   (1/0)
clindata$pf_parasites <- NA
clindata$pf_parasites[which(as.numeric(as.character(clindata$curated_falciparum_parasitemia)) == 0)] <-0
clindata$pf_parasites[which(as.numeric(as.character(clindata$curated_falciparum_parasitemia)) >  0)] <-1
table(clindata$pf_parasites, exclude = NULL)
table(clindata$pf_parasites,clindata$caseorcontrol, useNA="ifany")
table(clindata$pf_parasites, clindata$malaria_positive, exclude = NULL)

# Non-falciparum parasitemia present/absent   (1/0) - PNG only
clindata$nonpf_parasites <- NA
clindata$nonpf_parasites[which(as.numeric(as.character(clindata$non_falciparum_parasitemia_parasites_ul)) == 0)] <-0
clindata$nonpf_parasites[which(as.numeric(as.character(clindata$non_falciparum_parasitemia_parasites_ul)) >  0)] <-1
table(clindata$nonpf_parasites, exclude = NULL)
table(clindata$nonpf_parasites,clindata$caseorcontrol, useNA="ifany")
table(clindata$nonpf_parasites, clindata$malaria_positive, exclude = NULL)

# Log10 parasitemia with 0 parasitemias recoded as NA
str(clindata$curated_falciparum_parasitemia)
clindata$curated_falciparum_parasitemia <- as.numeric(as.character(clindata$curated_falciparum_parasitemia))
clindata$logpospara <- ifelse(clindata$curated_falciparum_parasitemia > 0, log10(clindata$curated_falciparum_parasitemia), NA)

# Log10 parasitemia with 0 parasitemias recoded as 0
clindata$logallpara <- ifelse(clindata$curated_falciparum_parasitemia > 0, log10(clindata$curated_falciparum_parasitemia), 0 )

#------------------------------------------------------------------------------------------
#***other_diagnosis
#------------------------------------------------------------------------------------------
clindata$other_diagnosis <- NA
# lookup columns with details of other diagnoses
others <- grep("other_diagnosis_", names(clindata)); nothers <- names(clindata)[others]; cbind(others, nothers)
# create combined field for other_diagnosis
clindata$other_diagnosis[clindata[,"other_diagnosis_respiratory_infection"] == "YES" | clindata[,"other_diagnosis_unknown"] == "YES" | clindata[,"other_diagnosis_malnutrition"] == "YES" | clindata[,"other_diagnosis_meningitis"] == "YES" | clindata[,"other_diagnosis_other"] == "YES" | clindata[,"other_diagnosis_sickle_cell"] == "YES" | clindata[,"other_diagnosis_gastroenteritis"] == "YES"] <- 1
clindata$other_diagnosis[clindata[,"other_diagnosis_respiratory_infection"] == "NO"  & clindata[,"other_diagnosis_unknown"] == "NO"  & clindata[,"other_diagnosis_malnutrition"] == "NO"  & clindata[,"other_diagnosis_meningitis"] == "NO"  & clindata[,"other_diagnosis_other"] == "NO"  & clindata[,"other_diagnosis_sickle_cell"] == "NO"  & clindata[,"other_diagnosis_gastroenteritis"] == "NO" ] <- 0
table(clindata$other_diagnosis, useNA = "ifany")
#also create a field called other_diagnosis_names based on what they have
clindata$other_diagnosis_names <- NA
clindata$other_diagnosis_names[clindata[,"other_diagnosis_malnutrition"] == "YES"]             <- "MALNUTRITION"
clindata$other_diagnosis_names[clindata[,"other_diagnosis_meningitis"] == "YES"]               <- "MENINGITIS"
clindata$other_diagnosis_names[clindata[,"other_diagnosis_other"] == "YES"]                    <- "OTHER"
clindata$other_diagnosis_names[clindata[,"other_diagnosis_respiratory_infection"] == "YES"]    <- "RESP_INF"
clindata$other_diagnosis_names[clindata[,"other_diagnosis_sickle_cell"] == "YES"]              <- "SICKLE"
clindata$other_diagnosis_names[clindata[,"other_diagnosis_unknown"] == "YES"]                  <- "UNKNOWN"
clindata$other_diagnosis_names[clindata[,"other_diagnosis_gastroenteritis"] == "YES"] <- "GASTROENT"
table(clindata$other_diagnosis_names)

clindata$other_diagnosis_use <- NA
clindata$other_diagnosis_use[clindata$other_diagnosis == 1 | clindata$other_diagnosis == 0 ] <- 0
#adjust as necessay
clindata$other_diagnosis_use[clindata[,"other_diagnosis_respiratory_infection"] == "YES" | clindata[,"other_diagnosis_unknown"] == "YES" | clindata[,"other_diagnosis_malnutrition"] == "YES" | clindata[,"other_diagnosis_meningitis"] == "YES" | clindata[,"other_diagnosis_other"] == "YES" | clindata[,"other_diagnosis_sickle_cell"] == "YES" | clindata[,"other_diagnosis_gastroenteritis"] == "YES"] <- 1
table(clindata$other_diagnosis_use,clindata$other_diagnosis_names)


#------------------------------------------------------------------------------------------
#***cleaned_case  - based on parasite status
#------------------------------------------------------------------------------------------

table(clindata$study_code, clindata$caseorcontrol)

#**CASE with P.f parasites  (1/0)
clindata$cleaned_case <- NA
clindata$cleaned_case[clindata$caseorcontrol == "CASE"] <- 0
clindata$cleaned_case[clindata$caseorcontrol == "CASE" & (clindata$pf_parasites == 1 | clindata$malaria_positive == "YES") ] <- 1
table(clindata$cleaned_case, useNA = "ifany")
table(clindata$cleaned_case, clindata$caseorcontrol, useNA = "ifany")

#------------------------------------------------------------------------------------------
#***cleaned_control - based on parasite status
#------------------------------------------------------------------------------------------
#**CONTROL without P.f or non-P.f parasites   (1/0)
clindata$cleaned_control <- NA
clindata$cleaned_control [clindata$caseorcontrol == "CONTROL"] <- 1
clindata$cleaned_control [clindata$caseorcontrol == "CONTROL" & (clindata$pf_parasites == 1 | clindata$nonpf_parasites == 1 | clindata$malaria_positive == "YES")] <- 0
table(clindata$cleaned_control, useNA = "ifany")
table(clindata$cleaned_control, clindata$caseorcontrol, useNA = "ifany")

#------------------------------------------------------------------------------------------
#***Hyperparasitaemia
#------------------------------------------------------------------------------------------


#**Parasitaemia >10,000
clindata$hyper_parasit10000 <- NA
clindata$hyper_parasit10000[is.na(clindata$pf_parasites) == FALSE] <-0
clindata$hyper_parasit10000[as.numeric(as.character(clindata$curated_falciparum_parasitemia)) > 10000] <-1
table(clindata$hyper_parasit10000,clindata$caseorcontrol, useNA="ifany")
#check
clindata[1:20,c("hyper_parasit10000","parasitemia_parasites_ul")]

#let's add a hyperhyperparasitaemia
#**Parasitaemia >100,000
clindata$hyper_parasit100000 <- NA
clindata$hyper_parasit100000[is.na(clindata$pf_parasites) == FALSE] <-0
clindata$hyper_parasit100000[as.numeric(as.character(clindata$curated_falciparum_parasitemia)) > 100000] <-1
table(clindata$hyper_parasit100000,clindata$caseorcontrol, useNA="ifany")
#check
clindata[1:20,c("hyper_parasit100000","parasitemia_parasites_ul")]

#let's add a hyperhyperparasitaemia
#**Parasitaemia >500,000
clindata$hyper_parasit500000 <- NA
clindata$hyper_parasit500000[is.na(clindata$pf_parasites) == FALSE] <-0
clindata$hyper_parasit500000[as.numeric(as.character(clindata$curated_falciparum_parasitemia)) > 500000] <-1
table(clindata$hyper_parasit500000,clindata$caseorcontrol, useNA="ifany")
#check
clindata[1:20,c("hyper_parasit500000","curated_falciparum_parasitemia")]


#---------------------------------------------------------------------------------------------------------
# strict_caseorcontrol
#**Case and controls: cases are CASES with parasitemia (1) and controls are CONTROLS without parasitaemia (0)
#this leaves out CASES without parasites (could just be time of cycle or not-a-malaria-case - but will still be marked up in CASESORCONTROL field
#also drops controls with visible parasitaemias that could be classified as MILDMALARIA - but these are children running around 'healthy'
# and did not at that time require hospital treatment.
#---------------------------------------------------------------------------------------------------------
clindata$strict_caseorcontrol <- NA
clindata$strict_caseorcontrol[clindata$cleaned_case == 1] <- 1
clindata$strict_caseorcontrol[clindata$cleaned_control == 1] <- 0
table(clindata$strict_caseorcontrol, useNA="ifany")
table(clindata$strict_caseorcontrol,clindata$caseorcontrol, useNA="ifany")
table(clindata$strict_caseorcontrol,clindata$pf_parasites, useNA="ifany") # 9 individuals classed as cases without parasitemia- find these
clindata

## Create strict case/control/parent field in each file
clindata$strict_caseorcontrolparents <- clindata$strict_caseorcontrol
clindata$strict_caseorcontrolparents[clindata$caseorcontrol == "PARENT"] <- "Parent"
clindata$strict_caseorcontrolparents[clindata$caseorcontrol == "OTHER"]  <- "Other"
clindata$strict_caseorcontrolparents[clindata$strict_caseorcontrol == 0] <- "Control"
clindata$strict_caseorcontrolparents[clindata$strict_caseorcontrol == 1] <- "Case"
table(clindata$strict_caseorcontrolparents, clindata$country_name, exclude=NULL)


#--------------------------------------------------------------------------------------------
#***Blood glucose level
#--------------------------------------------------------------------------------------------
length(which(!is.na(clindata$glucose_mmol_l)))
str(clindata$glucose_mmol_l)
clindata$glucose_mmol_l <- as.numeric(as.character(clindata$glucose_mmol_l))
#hypoglycemia - blood glucose <3.9
clindata$hypog39 <- NA
#code
clindata$hypog39[clindata$glucose_mmol_l  <3.9] <- 1
clindata$hypog39[clindata$glucose_mmol_l >=3.9] <- 0
#clindata$hypog39[clindata$glucose_mmol_l >=3.9 & is.na(clindata$glucose_mmol_l)==FALSE] <- 0
#check
length(which(!is.na(clindata$glucose_mmol_l))); length(which(!is.na(clindata$hypog39)))
table(clindata$hypog39, exclude = NULL)
clindata[1:20,c("hypog39","glucose_mmol_l")]

#hypoglycemia - blood glucose <=2.1
clindata$hypog21 <- NA
clindata$hypog21[clindata$glucose_mmol_l  <2.1 ] <- 1
clindata$hypog21[clindata$glucose_mmol_l >=2.1 ] <- 0
#check
length(which(!is.na(clindata$glucose_mmol_l))); length(which(!is.na(clindata$hypog21)))
table(clindata$hypog21, exclude = NULL)
clindata[1:20,c("hypog21","glucose")]

#view
table(clindata$hypog39,useNA="ifany")
table(clindata$hypog21,useNA="ifany")

#----------------------------------------------------------------------------------------------
# Align BCS and GCS to coma_status_XXXXX
# this metric is to define whether an individual has a score sufficient to say they have coma
# other conditions can be added afterwards - such as glucose etc
#**BCS    either <=3  or <=2
#**GCS    either <=9  or <=2
#**if both BCS and GCS measured then use BCS as BCS is a metric more appropriate to children than GCS
#we will define variables to simply catagorise individuals as coma or not for above individula metrics
#then combine to make our coma_status_XXXX
#----------------------------------------------------------------------------------------------
table(clindata$blantyre_coma_score, useNA="ifany")
str(clindata$blantyre_coma_score)
clindata$blantyre_coma_score <- as.numeric(as.character(clindata$blantyre_coma_score))
table(clindata$glasgow_coma_score, useNA="ifany")
str(clindata$glasgow_coma_score)
clindata$glasgow_coma_score <- as.numeric(as.character(clindata$glasgow_coma_score))
#BCS<=3
clindata$bcs3 <- NA
#code
clindata$bcs3[clindata$blantyre_coma_score <= 3] <- 1
clindata$bcs3[clindata$blantyre_coma_score  > 3] <- 0
table(clindata$bcs3, useNA="ifany")

#GCS <=9
clindata$gcs9 <- NA
#code
clindata$gcs9[clindata$glasgow_coma_score <= 9] <- 1
clindata$gcs9[clindata$glasgow_coma_score  > 9] <- 0
table(clindata$gcs9, useNA="ifany")

#BCS<=2
clindata$bcs2 <- NA
#code
clindata$bcs2[clindata$blantyre_coma_score <= 2] <- 1
clindata$bcs2[clindata$blantyre_coma_score  > 2] <- 0
table(clindata$bcs2, useNA="ifany")

#GCS <=8
clindata$gcs8 <- NA
#code
clindata$gcs8[clindata$glasgow_coma_score <= 8] <- 1
clindata$gcs8[clindata$glasgow_coma_score  > 8] <- 0
table(clindata$gcs8, useNA="ifany")


#coma_status  BCS3 or GCS9
clindata$coma_status_3_9 <- NA
#code 
#apply GCS first then overlay BCS
clindata$coma_status_3_9[clindata$gcs9 == 1 | clindata$bcs3 == 1] <- 1
clindata$coma_status_3_9[clindata$gcs9 == 0 & (clindata$bcs3 == 0 | is.na(clindata$bcs3) == TRUE)] <- 0
clindata$coma_status_3_9[clindata$bcs3 == 0 & (clindata$gcs9 == 0 | is.na(clindata$gcs9) == TRUE)] <- 0
table(clindata$coma_status_3_9, useNA="ifany")



#coma_status  BCS2 or GCS8
clindata$coma_status_2_8 <- NA
#code 
#apply GCS first then overlay BCS
clindata$coma_status_2_8[clindata$gcs8 == 1 | clindata$bcs2 == 1] <- 1
clindata$coma_status_2_8[clindata$gcs8 == 0 & (clindata$bcs2 == 0 | is.na(clindata$bcs2) == TRUE)] <- 0
clindata$coma_status_2_8[clindata$bcs2 == 0 & (clindata$gcs8 == 0 | is.na(clindata$gcs9) == TRUE)] <- 0
table(clindata$coma_status_2_8, useNA="ifany")

table(clindata$coma_status_3_9,clindata$coma_status_2_8, useNA="ifany")



#--------------------------------------------------------------------------------------------
#***Hypoglycemia_status
#--------------------------------------------------------------------------------------------

#Hypoglycemia status based on hypog<=2.1 and bcs_assessed30_min_after_convulsions_blood_sugar21mmperl
table(clindata$hypog21, clindata$bcs_assessed30_min_after_convulsions_blood_sugar_2_1mm_l, exclude = NULL)
clindata$hypoglyc_status <- NA
#code
clindata$hypoglyc_status[clindata$hypog21 ==0 | clindata$bcs_assessed30_min_after_convulsions_blood_sugar_2_1mm_l=="YES"]         <- 1
clindata$hypoglyc_status[clindata$bcs_assessed30_min_after_convulsions_blood_sugar_2_1mm_l=="NO" & clindata$hypog21 ==1]          <- 0
clindata$hypoglyc_status[clindata$bcs_assessed30_min_after_convulsions_blood_sugar_2_1mm_l=="NO" & is.na(clindata$hypog21)==TRUE] <- 0
table(clindata$hypoglyc_status, useNA="ifany")


#############################################################################################################################################################
#############################################################################################################################################################

##NB: [Jen] Many of the fields used in this section previously were surrounded by as.numeric(as.character()) but I have removed these as str() should show these fields to be numeric and if they're not something has gone wrong when they were generated!

#Now build the CM phenotypes
#cm1_28 is a strict case with a coma based on BCS2 and GCS8
#This code now includes the cerebral malaria cases from Dave Roberts Kenyan group where only info on cerebral malaria yes/no was present (no BSC etc to base it on)
table(clindata$strict_caseorcontrol,clindata$coma_status_2_8)
clindata$cm1_28 <- NA
#code
clindata$cm1_28[clindata$strict_caseorcontrol == 1 & clindata$coma_status_2_8== 1] <- 1
clindata$cm1_28[clindata$strict_caseorcontrol == 1 & clindata$cerebral_malaria_status=="YES"] <- 1
clindata$cm1_28[clindata$strict_caseorcontrol == 1 & clindata$coma_status_2_8== 0] <- 0
table(clindata$cm1_28, useNA="ifany")
table(clindata$strict_caseorcontrol,clindata$cm1_28, useNA="ifany")

#cm1_39 is a strict case with a coma based on BCS3 and GCS3
#This code now includes the cerebral malaria cases from Dave Roberts Kenyan group where only info on cerebral malaria yes/no was present (no BSC etc to base it on)
table(clindata$strict_caseorcontrol,clindata$coma_status_3_9, useNA="ifany")
clindata$cm1_39 <- NA
#code
clindata$cm1_39[clindata$strict_caseorcontrol == 1 & clindata$coma_status_3_9 == 1] <- 1
clindata$cm1_39[clindata$strict_caseorcontrol == 1 & clindata$cerebral_malaria_status =="YES"] <- 1
clindata$cm1_39[clindata$strict_caseorcontrol == 1 & clindata$coma_status_3_9 == 0] <- 0
table(clindata$cm1_39, useNA="ifany")
table(clindata$strict_caseorcontrol,clindata$cm1_39, useNA="ifany")


#cm2_28 is a strict case with a coma based on BCS2 and GCS8 AND is NOT hypoglycemic
#This code now includes the cerebral malaria cases and hypog status from Dave Roberts Kenyan group where only info on cerebral malaria yes/no and hyprogylcemia yes/no was present (no BSC etc to base it on)
clindata$cm2_28 <- NA
#code
clindata$cm2_28[clindata$cm1_28 == 1 & clindata$hypoglyc_status==1] <- 1
clindata$cm2_28[clindata$cm1_28 == 1 & clindata$hypoglycaemia == "NO"] <- 1
clindata$cm2_28[clindata$cm1_28 == 1 & clindata$hypoglyc_status==0] <- 0
table(clindata$cm2_28, useNA="ifany")
table(clindata$strict_caseorcontrol,clindata$cm2_28, useNA="ifany")


#cm2_39 is a strict case with a coma based on BCS3 and GCS9 AND is NOT hypoglycemic
clindata$cm2_39 <- NA
#code
clindata$cm2_39[clindata$cm1_39 == 1 & clindata$hypoglyc_status==1] <- 1
clindata$cm2_39[clindata$cm1_39 == 1 & clindata$hypoglycaemia == "NO"] <- 1
clindata$cm2_39[clindata$cm1_39 == 1 & clindata$hypoglyc_status==0] <- 0
table(clindata$cm2_39, useNA="ifany")
table(clindata$strict_caseorcontrol,clindata$cm1_39, useNA="ifany")




#cm1_28_control; severe malaria cases as for cm1 but here 0=strict_caseorcontrol
clindata$cm1_28_control <- NA
#code
clindata$cm1_28_control[clindata$cm1_28== 1] <- 1
clindata$cm1_28_control[clindata$strict_caseorcontrol == 0] <- 0
table(clindata$cm1_28_control, useNA="ifany")
table(clindata$strict_caseorcontrol,clindata$cm1_28_control, useNA="ifany")



#cm1_39_control; severe malaria cases as for cm1 but here 0=strict_caseorcontrol
clindata$cm1_39_control <- NA
#code
clindata$cm1_39_control[clindata$cm1_39== 1] <- 1
clindata$cm1_39_control[clindata$strict_caseorcontrol == 0] <- 0
table(clindata$cm1_39_control, useNA="ifany")
table(clindata$strict_caseorcontrol,clindata$cm1_39_control, useNA="ifany")



#cm2_28_control; severe malaria cases as for cm1 but here 0=strict_caseorcontrol
clindata$cm2_28_control <- NA
#code
clindata$cm2_28_control[clindata$cm2_28==1] <- 1
clindata$cm2_28_control[clindata$strict_caseorcontrol == 0] <- 0
table(clindata$cm2_28_control, useNA="ifany")
table(clindata$strict_caseorcontrol,clindata$cm2_28_control, useNA="ifany")



#cm2_39_control; severe malaria cases as for cm1 but here 0=strict_caseorcontrol
clindata$cm2_39_control <- NA
datdic[which(names(clindata) == "cm2_39_control"), 1:3] <- c (which(names(clindata) == "cm2_39_control"), "cm2_39_control", "Cerebral malaria 2_39: strict case with a coma (based on BCS<=3 and GCS<=9) AND not hypoglycemic: 0=Strict control; 1=Case with cm2 traits")
#code
clindata$cm2_39_control[clindata$cm2_39==1] <- 1
clindata$cm2_39_control[clindata$strict_caseorcontrol == 0] <- 0
table(clindata$cm2_39_control, useNA="ifany")
table(clindata$strict_caseorcontrol,clindata$cm2_39_control, useNA="ifany")





#---------------------------------------------------------------------------------------------------------------------
#**haemoglobin
#---------------------------------------------------------------------------------------------------------------------

table(clindata$haemoglobin_g_dl, useNA="ifany")
str(clindata$haemoglobin_g_dl)
clindata$haemoglobin_g_dl <- as.numeric(as.character(clindata$haemoglobin_g_dl))

#haemoglogin <5
clindata$haem_5 <- NA
#code
clindata$haem_5[clindata$haemoglobin_g_dl <5 ] <- 1
clindata$haem_5[clindata$haemoglobin_g_dl >=5 ] <- 0
table(clindata$haem_5, useNA="ifany")
table(clindata$strict_caseorcontrol,clindata$haem_5, useNA="ifany")


#haemoglogin <6
clindata$haem_6 <- NA
#code
clindata$haem_6[clindata$haemoglobin_g_dl <6 ] <- 1
clindata$haem_6[clindata$haemoglobin_g_dl >=6] <- 0
table(clindata$haem_6, useNA="ifany")
table(clindata$strict_caseorcontrol,clindata$haem_6, useNA="ifany")



#---------------------------------------------------------------------------------------------------------------------
#**hematocrit
#---------------------------------------------------------------------------------------------------------------------

summary(as.numeric(as.character(clindata$hematocrit)))
# hematocrit field has been classed as a factor as is a mix of whole numbers & decimals, so can therefore be changed back to numeric.
clindata$hematocrit <- as.numeric(as.character(clindata$hematocrit))

#hematocrit <15
clindata$hemat_15 <- NA
#code
clindata$hemat_15[clindata$hematocrit <15 ] <- 1
clindata$hemat_15[clindata$hematocrit >=15] <- 0
table(clindata$hemat_15, useNA="ifany")
table(clindata$strict_caseorcontrol,clindata$hemat_15, useNA="ifany")

#hematocrit <18
clindata$hemat_18 <- NA
#code
clindata$hemat_18[clindata$hematocrit <18 ] <- 1
clindata$hemat_18[clindata$hematocrit >=18] <- 0
table(clindata$hemat_18, useNA="ifany")
table(clindata$strict_caseorcontrol,clindata$hemat_18, useNA="ifany")


#---------------------------------------------------------------------------------------------------------------------
#**haemoglobin or, hematocrit if Hb not available
#---------------------------------------------------------------------------------------------------------------------

#haemoglobin or, hematocrit if Hb not available
clindata$haem5_hemat15 <- NA
#code
clindata$haem5_hemat15[clindata$haem_5 ==1] <- 1
clindata$haem5_hemat15[clindata$hemat_15 == 1 & is.na(clindata$haem_5) == TRUE] <- 1
clindata$haem5_hemat15[clindata$haem_5 == 0] <- 0
clindata$haem5_hemat15[clindata$hemat_15 == 0 & is.na(clindata$haem_5) == TRUE] <- 0
table(clindata$haem5_hemat15, useNA="ifany")
table(clindata$strict_caseorcontrol,clindata$haem5_hemat15, useNA="ifany")

clindata$haem6_hemat18 <- NA
#code
clindata$haem6_hemat18[clindata$haem_6 ==1] <- 1
clindata$haem6_hemat18[clindata$hemat_18 == 1 & is.na(clindata$haem_6) == TRUE] <- 1
clindata$haem6_hemat18[clindata$haem_6 == 0] <- 0
clindata$haem6_hemat18[clindata$hemat_18 == 0 & is.na(clindata$haem_6) == TRUE] <- 0
table(clindata$haem6_hemat18, useNA="ifany")
table(clindata$strict_caseorcontrol,clindata$haem6_hemat18, useNA="ifany")


#Curated haemoglobin using haematocrit/3
#now calculate haemoglobin for sites with only haematocrit
str(clindata$haemoglobin_g_dl)
clindata$curated_hb <- as.numeric(as.character(clindata$haemoglobin_g_dl))
str(clindata$hematocrit)
clindata$curated_hb[is.na(clindata$haemoglobin_g_dl) & !is.na(clindata$hematocrit)] <- clindata$hematocrit[is.na(clindata$haemoglobin_g_dl) & !is.na(clindata$hematocrit)]/3
length(which(!is.na(clindata$haemoglobin_g_dl))); length(which(!is.na(clindata$hematocrit))); length(which(!is.na(clindata$curated_hb)))
hist(clindata$curated_hb, breaks = 100)

#######################################################################################################################
#---------------------------------------------------------------------------------------------------------------------
#**Severe Anaemia phenotypes
#---------------------------------------------------------------------------------------------------------------------

#Severe anaemia; Strict case and haem<5 or haematocrit<15 and 0=cases without trait
clindata$sma5_15<- NA
#code
clindata$sma5_15[clindata$strict_caseorcontrol==1 & (clindata$haem5_hemat15 ==0 | clindata$anaemia_status == 'NO' )] <- 0
clindata$sma5_15[clindata$strict_caseorcontrol==1 & (clindata$haem5_hemat15 ==1 | clindata$anaemia_status == 'YES')] <- 1
table(clindata$sma5_15, useNA="ifany")
table(clindata$strict_caseorcontrol,clindata$sma5_15, useNA="ifany")


#Severe anaemia; Strict case and haem<5 or haematocrit<15 0=cases without trait
clindata$sma6_18<- NA
#code
clindata$sma6_18[clindata$strict_caseorcontrol==1 & clindata$haem6_hemat18 ==1] <- 1
clindata$sma6_18[clindata$strict_caseorcontrol==1 & clindata$haem6_hemat18 ==0] <- 0
table(clindata$sma6_18, useNA="ifany")
table(clindata$strict_caseorcontrol,clindata$sma6_18, useNA="ifany")


#Severe anaemia; Strict case and haem<5 or haematocrit<15 and 0=strict controls
clindata$sma5_15_control <- NA
#code
clindata$sma5_15_control[clindata$strict_caseorcontrol==1 & (clindata$haem5_hemat15 ==1 | clindata$anaemia_status == 'YES')] <- 1
clindata$sma5_15_control[clindata$strict_caseorcontrol==0] <- 0
table(clindata$sma5_15_control, useNA="ifany")
table(clindata$strict_caseorcontrol,clindata$sma5_15_control, useNA="ifany")


#Severe anaemia; Strict case and haem<5 or haematocrit<15 0=strict controls
clindata$sma6_18_control <- NA
#code
clindata$sma6_18_control[clindata$strict_caseorcontrol==1 & clindata$haem6_hemat18 ==1] <- 1
clindata$sma6_18_control[clindata$strict_caseorcontrol==0] <- 0
table(clindata$sma6_18_control, useNA="ifany")
table(clindata$strict_caseorcontrol,clindata$sma6_18_control, useNA="ifany")


#---------------------------------------------------------------------------------------------------------------------
#**Died
#---------------------------------------------------------------------------------------------------------------------

clindata$died <-NA
#code
clindata$died[clindata$outcome == toupper("Died")] <- 1
clindata$died[clindata$outcome == toupper("Survived")] <- 0
clindata$died[clindata$outcome == toupper("Absconded")] <- NA
table(clindata$died, useNA="ifany")
table(clindata$strict_caseorcontrol,clindata$died, useNA="ifany")

#----------------------------------------
#strict case that died
#--------------------------------------------

clindata$died_case <- NA
#code
clindata$died_case[clindata$strict_caseorcontrol == 1 & clindata$died== 1] <- 1
clindata$died_case[clindata$strict_caseorcontrol == 1 & clindata$died== 0] <- 0
table(clindata$died_case)
table(clindata$died, clindata$strict_caseorcontrol, useNA="ifany")
table(clindata$strict_caseorcontrol,clindata$died_case, useNA="ifany")


#----------------------------------------
#strict case that died where 0=strict controls
#--------------------------------------------
clindata$died_case_control <-NA
#code
clindata$died_case_control[clindata$died_case ==1] <- 1
clindata$died_case_control[clindata$strict_caseorcontrol ==0] <- 0
table(clindata$died_case_control, useNA="ifany")
table(clindata$strict_caseorcontrol,clindata$died_case_control, useNA="ifany")



########################################################################################################################################
########################################################################################################################################

#Combinations of the phenotypes created above but using the cm_28 and haem_5_15 phenotypes.

#######################################################################################################################################

#---------------------------------------------------------------------------------------------------------------------
#*cm1 or sma - cerebral malaria (bcs<2 or gcs<8 and parasites) severe anaemia malaria (haemoglobin<5 or hematocrit<15 and parasites) 
#---------------------------------------------------------------------------------------------------------------------
clindata$cm1orsma <-NA
#code
clindata$cm1orsma[clindata$sma5_15 ==1 | (clindata$cm1_28)==1] <- 1
clindata$cm1orsma[clindata$sma5_15 ==0 & (clindata$cm1_28)==0] <- 0
table(clindata$cm1orsma, useNA="ifany")
table(clindata$strict_caseorcontrol,clindata$cm1orsma, useNA="ifany")



#---------------------------------------------------------------------------------------------------------------------
#*cm2 or sma - cerebral malaria (bcs<2 or gcs<8 and parasites ) severe anaemia malaria (haemoglobin< or hematocrit<15 and parasites)and NOT hypoglycemic
#---------------------------------------------------------------------------------------------------------------------
clindata$cm2orsma <-NA
#code
clindata$cm2orsma[clindata$sma5_15 ==1 | clindata$cm2_28 ==1] <- 1
clindata$cm2orsma[clindata$sma5_15 ==0 & clindata$cm2_28 ==0] <- 0
table(clindata$cm2orsma, useNA="ifany")
table(clindata$strict_caseorcontrol,clindata$cm2orsma, useNA="ifany")


#---------------------------------------------------------------------------------------------------------------------
#*cm1 NOT sma 
#---------------------------------------------------------------------------------------------------------------------
clindata$cm1notsma <-NA
#code
clindata$cm1notsma[clindata$cm1_28 ==1 & clindata$sma5_15 ==0] <- 1
clindata$cm1notsma[clindata$cm1_28 ==0 | clindata$sma5_15 ==1] <- 0
table(clindata$cm1notsma, useNA="ifany")
#check
table(clindata$cm1_28, clindata$sma5_15, useNA="ifany")
table(clindata$strict_caseorcontrol,clindata$cm1notsma, useNA="ifany")



#---------------------------------------------------------------------------------------------------------------------
#*cm2 NOT sma 
#---------------------------------------------------------------------------------------------------------------------
clindata$cm2notsma <-NA
#code
clindata$cm2notsma[clindata$cm2_28 ==1 & clindata$sma5_15 ==0] <- 1
clindata$cm2notsma[clindata$cm2_28 ==0 | clindata$sma5_15 ==1] <- 0
table(clindata$cm2notsma, useNA="ifany")
#check
table(clindata$cm2_28, clindata$sma5_15, useNA="ifany")
table(clindata$strict_caseorcontrol,clindata$cm2notsma, useNA="ifany")



#---------------------------------------------------------------------------------------------------------------------
#*sma NOT cm1
#---------------------------------------------------------------------------------------------------------------------
clindata$smanotcm1 <-NA
#code
clindata$smanotcm1[clindata$cm1_28==0 & clindata$sma5_15==1] <- 1
clindata$smanotcm1[clindata$cm1_28==1 | clindata$sma5_15==0] <- 0
table(clindata$smanotcm1, useNA="ifany")
#check
table(clindata$cm1_28, clindata$sma5_15, useNA="ifany")
table(clindata$strict_caseorcontrol,clindata$smanotcm1, useNA="ifany")



#---------------------------------------------------------------------------------------------------------------------
#*sma NOT cm2
#---------------------------------------------------------------------------------------------------------------------
clindata$smanotcm2 <-NA
#code
clindata$smanotcm2[clindata$cm2_28==0 & clindata$sma5_15==1] <- 1
clindata$smanotcm2[clindata$cm2_28==1 | clindata$sma5_15==0] <- 0
table(clindata$smanotcm2, useNA="ifany")
#check
table(clindata$cm2_28, clindata$sma5_15, useNA="ifany")
table(clindata$strict_caseorcontrol,clindata$smanotcm2, useNA="ifany")


#---------------------------------------------------------------------------------------------------------------------
#*cm1 AND sma
#---------------------------------------------------------------------------------------------------------------------
clindata$cm1andsma <-NA
#code
clindata$cm1andsma[clindata$cm1_28==1 & clindata$sma5_15==1] <- 1
clindata$cm1andsma[clindata$cm1_28==0 | clindata$sma5_15==0] <- 0
table(clindata$cm1andsma, useNA="ifany")
#check
table(clindata$cm1_28, clindata$sma5_15, useNA="ifany")
table(clindata$strict_caseorcontrol,clindata$cm1andsma, useNA="ifany")


#---------------------------------------------------------------------------------------------------------------------
#*cm2 AND sma
#---------------------------------------------------------------------------------------------------------------------
clindata$cm2andsma <-NA
#code
clindata$cm2andsma[clindata$cm2_28==1 & clindata$sma5_15==1] <- 1
clindata$cm2andsma[clindata$cm2_28==0 | clindata$sma5_15==0] <- 0
table(clindata$cm2andsma, useNA="ifany")
#check
table(clindata$cm2_28, clindata$sma5_15, useNA="ifany")
table(clindata$strict_caseorcontrol,clindata$cm2andsma, useNA="ifany")


#---------------------------------------------------------------------------------------------------------------------
#** cm1orsma_control
#---------------------------------------------------------------------------------------------------------------------
#The below counts ONLY controls (ie controls from the strict_caseorcontrol variable whereas the 'cm1orsma' variable
#includes all of the cases that are NOT cm1 or sma. The same goes for the other phenotypes labeled similarly. 
#Need to ask Kirk if the controls for 'cm1orsma' should all be cases? And exclude the true controls...
clindata$cm1orsma_control <-NA
#code
clindata$cm1orsma_control[clindata$cm1orsma==1] <- 1
clindata$cm1orsma_control[clindata$strict_caseorcontrol==0] <- 0
table(clindata$cm1orsma_control, useNA="ifany")
table(clindata$cm1orsma, useNA="ifany")
table(clindata$strict_caseorcontrol,clindata$cm1orsma_control, useNA="ifany")


#---------------------------------------------------------------------------------------------------------------------
#** cm2orsma_control
#---------------------------------------------------------------------------------------------------------------------
clindata$cm2orsma_control <-NA
#code
clindata$cm2orsma_control[clindata$cm2orsma==1] <- 1
clindata$cm2orsma_control[clindata$strict_caseorcontrol==0] <- 0
table(clindata$cm2orsma_control, useNA="ifany")
table(clindata$cm2orsma, useNA="ifany")
table(clindata$strict_caseorcontrol,clindata$cm2orsma_control, useNA="ifany")

#---------------------------------------------------------------------------------------------------------------------
#** cm1notsma_control
#---------------------------------------------------------------------------------------------------------------------
clindata$cm1notsma_control <-NA
#code
clindata$cm1notsma_control[clindata$cm1notsma==1] <- 1
clindata$cm1notsma_control[clindata$strict_caseorcontrol==0] <- 0
table(clindata$cm1notsma_control, useNA="ifany")
table(clindata$cm1notsma, useNA="ifany")

#---------------------------------------------------------------------------------------------------------------------
#** cm2notsma_control
#---------------------------------------------------------------------------------------------------------------------
clindata$cm2notsma_control <-NA
#code
clindata$cm2notsma_control[clindata$cm2notsma==1] <- 1
clindata$cm2notsma_control[clindata$strict_caseorcontrol==0] <- 0
table(clindata$cm2notsma_control, useNA="ifany")
table(clindata$cm2notsma, useNA="ifany")


#---------------------------------------------------------------------------------------------------------------------
#** smanotcm1_control
#---------------------------------------------------------------------------------------------------------------------
clindata$smanotcm1_control <-NA
#code
clindata$smanotcm1_control[clindata$smanotcm1==1] <- 1
clindata$smanotcm1_control[clindata$strict_caseorcontrol==0] <- 0
table(clindata$smanotcm1_control, useNA="ifany")
table(clindata$smanotcm1, useNA="ifany")

#---------------------------------------------------------------------------------------------------------------------
#** smanotcm2_control
#---------------------------------------------------------------------------------------------------------------------
clindata$smanotcm2_control <-NA
#code
clindata$smanotcm2_control[clindata$smanotcm2==1] <- 1
clindata$smanotcm2_control[clindata$strict_caseorcontrol==0] <- 0
table(clindata$smanotcm2_control, useNA="ifany")
table(clindata$smanotcm2, useNA="ifany")

#---------------------------------------------------------------------------------------------------------------------
#** cm1andsma_control
#---------------------------------------------------------------------------------------------------------------------
clindata$cm1andsma_control <-NA
#code
clindata$cm1andsma_control[clindata$cm1andsma==1] <- 1
clindata$cm1andsma_control[clindata$strict_caseorcontrol==0] <- 0
table(clindata$cm1andsma_control, useNA="ifany")
table(clindata$cm1andsma, useNA="ifany")

#---------------------------------------------------------------------------------------------------------------------
#** cm2andsma_control
#---------------------------------------------------------------------------------------------------------------------
clindata$cm2andsma_control <-NA
#code
clindata$cm2andsma_control[clindata$cm2andsma==1] <- 1
clindata$cm2andsma_control[clindata$strict_caseorcontrol==0] <- 0
table(clindata$cm2andsma_control)
table(clindata$cm2andsma)

#----------------------------------------
#Strict cases not cm1_28 not sma
#--------------------------------------------
clindata$casenotcm1notsma <-NA
#code
clindata$casenotcm1notsma[clindata$cm1_28==0 & clindata$sma5_15==0 ] <- 1
clindata$casenotcm1notsma[clindata$cm1_28==1 | clindata$sma5_15==1 ] <- 0
table(clindata$casenotcm1notsma, useNA="ifany")


#----------------------------------------
#Strict cases not cm1_28 not sma where 0=strict controls
#--------------------------------------------
clindata$casenotcm1notsma_control <-NA
#code
clindata$casenotcm1notsma_control[clindata$casenotcm1notsma ==1] <- 1
clindata$casenotcm1notsma_control[clindata$strict_caseorcontrol==0] <- 0
table(clindata$casenotcm1notsma_control, useNA="ifany")


#---------------------------------------------------------------------------------------------------------------------
#** died_cm1_28
#---------------------------------------------------------------------------------------------------------------------
#died and has cm1_28 
clindata$died_cm1_28 <- NA
#code
clindata$died_cm1_28[clindata$cm1_28 == 1 & clindata$died== 1] <- 1
clindata$died_cm1_28[clindata$cm1_28 == 1 & clindata$died== 0] <- 0
table(clindata$died_cm1_28, useNA="ifany")
table(clindata$died, clindata$cm1_28, useNA="ifany")


#---------------------------------------------------------------------------------------------------------------------
#** died_cm2_28
#---------------------------------------------------------------------------------------------------------------------
#Died and has cm2_28
clindata$died_cm2_28 <- NA
#code
clindata$died_cm2_28[clindata$cm2_28 == 1 & clindata$died == 1] <- 1
clindata$died_cm2_28[clindata$cm2_28 == 1 & clindata$died == 0] <- 0
table(clindata$died_cm2_28, useNA="ifany")
table(clindata$died, clindata$cm2_28, useNA="ifany")

#---------------------------------------------------------------------------------------------------------------------
#** died_sma_5_15
#---------------------------------------------------------------------------------------------------------------------
#Died and has sma_5_15
clindata$died_sma_5_15 <- NA
#code
clindata$died_sma_5_15[clindata$sma5_15 == 1 & clindata$died== 1] <- 1
clindata$died_sma_5_15[clindata$sma5_15 == 1 & clindata$died== 0] <- 0
table(clindata$died_sma_5_15, useNA="ifany")
table(clindata$died, clindata$sma5_15, useNA="ifany")


#---------------------------------------------------------------------------------------------------------------------
#** died_control
#---------------------------------------------------------------------------------------------------------------------
#Died==1 Control==0
clindata$died_control <-NA
#code
clindata$died_control[clindata$died==1] <- 1
clindata$died_control[clindata$strict_caseorcontrol==0] <- 0
table(clindata$died_control, useNA="ifany")
table(clindata$died, useNA="ifany")


#---------------------------------------------------------------------------------------------------------------------
#** died_cm1_28_control
#---------------------------------------------------------------------------------------------------------------------
#died and has cm1_28 0=strict control
clindata$died_cm1_28_control <- NA
#code
clindata$died_cm1_28_control[clindata$cm1_28 == 1 & clindata$died== 1] <- 1
clindata$died_cm1_28_control[clindata$strict_caseorcontrol== 0] <- 0
table(clindata$died_cm1_28_control, useNA="ifany")
table(clindata$died_cm1_28, useNA="ifany")


#---------------------------------------------------------------------------------------------------------------------
#** died_cm2_28_control
#---------------------------------------------------------------------------------------------------------------------
#Died and has cm2_28_control
clindata$died_cm2_28_control <- NA
#code
clindata$died_cm2_28_control[clindata$cm2_28 == 1 & clindata$died== 1] <- 1
clindata$died_cm2_28_control[clindata$strict_caseorcontrol== 0] <- 0
table(clindata$died_cm2_28_control, useNA="ifany")
table(clindata$died_cm2_28, useNA="ifany")


#---------------------------------------------------------------------------------------------------------------------
#** died_sma5_15_control
#---------------------------------------------------------------------------------------------------------------------
#Died and has sma5_15_control
clindata$died_sma5_15_control <- NA
#code
clindata$died_sma5_15_control[clindata$sma5_15 == 1 & clindata$died== 1] <- 1
clindata$died_sma5_15_control[clindata$strict_caseorcontrol== 0] <- 0
table(clindata$died_sma5_15_control, useNA="ifany")
table(clindata$died_sma_5_15, useNA="ifany")


#---------------------------------------------------------------------------------------------------------------------
#** diedcm1orsma_control
#---------------------------------------------------------------------------------------------------------------------
clindata$diedcm1orsma_control <-NA
#code
clindata$diedcm1orsma_control[clindata$died==1 & clindata$cm1orsma==1] <- 1
clindata$diedcm1orsma_control[clindata$strict_caseorcontrol==0] <- 0
table(clindata$diedcm1orsma_control, useNA="ifany")



#---------------------------------------------------------------------------------------------------------------------
#** diedcm2orsma_control
#--------------------------------------------------------------------------------------------------------------------
clindata$diedcm2orsma_control <-NA
#code
clindata$diedcm2orsma_control[clindata$died==1 & clindata$cm2orsma==1] <- 1
clindata$diedcm2orsma_control[clindata$strict_caseorcontrol==0] <- 0
table(clindata$diedcm2orsma_control, useNA="ifany")

#---------------------------------------------------------------------------------------------------------------------
#** diedcm1notsma_control
#---------------------------------------------------------------------------------------------------------------------
clindata$diedcm1notsma__control <-NA
#code
clindata$diedcm1notsma__control[clindata$died==1 & clindata$cm1notsma==1] <- 1
clindata$diedcm1notsma__control[clindata$strict_caseorcontrol==0] <- 0
table(clindata$diedcm1notsma__control, useNA="ifany")

#---------------------------------------------------------------------------------------------------------------------
#** diedcm2notsma_control
#---------------------------------------------------------------------------------------------------------------------
clindata$diedcm2notsma__control <-NA
#code
clindata$diedcm2notsma__control[clindata$died==1 & clindata$cm2notsma==1] <- 1
clindata$diedcm2notsma__control[clindata$strict_caseorcontrol==0] <- 0
table(clindata$diedcm2notsma__control, useNA="ifany")

#---------------------------------------------------------------------------------------------------------------------
#** diedsmanotcm1_control
#---------------------------------------------------------------------------------------------------------------------
clindata$diedsmanotcm1_control <- NA
#code
clindata$diedsmanotcm1_control[clindata$died ==1 & clindata$smanotcm1 ==1] <-1 
clindata$diedsmanotcm1_control[clindata$strict_caseorcontrol==0] <- 0
table(clindata$diedsmanotcm1_control, useNA="ifany")

#---------------------------------------------------------------------------------------------------------------------
#** diedsmanotcm2_control
#---------------------------------------------------------------------------------------------------------------------
clindata$diedsmanotcm2_control <- NA
#code
clindata$diedsmanotcm2_control[clindata$died ==1 & clindata$smanotcm2 ==1] <-1 
clindata$diedsmanotcm2_control[clindata$strict_caseorcontrol==0] <- 0
table(clindata$diedsmanotcm2_control, useNA="ifany")
table(clindata$diedsmanotcm2, useNA="ifany")


#---------------------------------------------------------------------------------------------------------------------
#** diedcm1andsma_control
#---------------------------------------------------------------------------------------------------------------------
clindata$diedcm1andsma_control <- NA
#code
clindata$diedcm1andsma_control[clindata$died ==1 & clindata$cm1andsma ==1] <-1 
clindata$diedcm1andsma_control[clindata$strict_caseorcontrol==0] <- 0
table(clindata$diedcm1andsma_control, useNA="ifany")
table(clindata$diedcm1andsma, useNA="ifany")


#---------------------------------------------------------------------------------------------------------------------
#** diedcm2andsma_control
#---------------------------------------------------------------------------------------------------------------------
clindata$diedcm2andsma_control <- NA
#code
clindata$diedcm2andsma_control[clindata$died ==1 & clindata$cm2andsma ==1] <-1 
clindata$diedcm2andsma_control[clindata$strict_caseorcontrol==0] <- 0
table(clindata$diedcm2andsma_control, useNA="ifany")
table(clindata$diedcm2andsma, useNA="ifany")


########################################################################################################################################
########################################################################################################################################
#Combinations of the phenotypes created above but using the cm_39 and haem_5_15 phenotypes.
#######################################################################################################################################

#---------------------------------------------------------------------------------------------------------------------
#*cm1 or sma - cerebral malaria (bcs<2 or gcs<8 and parasites) severe anaemia malaria (haemoglobin<5 or hematocrit<15 and parasites) 
#---------------------------------------------------------------------------------------------------------------------
clindata$cm1_39_orsma <-NA
#code
clindata$cm1_39_orsma[clindata$sma5_15==1 | clindata$cm1_39==1] <- 1
clindata$cm1_39_orsma[clindata$sma5_15==0 & clindata$cm1_39==0] <- 0
table(clindata$cm1_39_orsma, useNA="ifany")


#---------------------------------------------------------------------------------------------------------------------
#*cm2 or sma - cerebral malaria (bcs<2 or gcs<8 and parasites ) severe anaemia malaria (haemoglobin< or hematocrit<15 and parasites)and NOT hypoglycemic
#---------------------------------------------------------------------------------------------------------------------
clindata$cm2_39_orsma <-NA
#code
clindata$cm2_39_orsma[clindata$sma5_15==1 | clindata$cm2_39==1] <- 1
clindata$cm2_39_orsma[clindata$sma5_15==0 & clindata$cm2_39==0] <- 0
table(clindata$cm2_39_orsma, useNA="ifany")


#---------------------------------------------------------------------------------------------------------------------
#*cm1 NOT sma 
#---------------------------------------------------------------------------------------------------------------------
clindata$cm1_39_notsma <-NA
#code
clindata$cm1_39_notsma[clindata$cm1_39 ==1 & clindata$sma5_15==0] <- 1
clindata$cm1_39_notsma[clindata$cm1_39 ==0 | clindata$sma5_15==1] <- 0
table(clindata$cm1_39_notsma, useNA="ifany")
#check
table(clindata$cm1_39, clindata$sma5_15, useNA="ifany")


#---------------------------------------------------------------------------------------------------------------------
#*cm2 NOT sma 
#---------------------------------------------------------------------------------------------------------------------
clindata$cm2_39_notsma <-NA
#code
clindata$cm2_39_notsma[clindata$cm2_39==1 & clindata$sma5_15==0] <- 1
clindata$cm2_39_notsma[clindata$cm2_39==0 | clindata$sma5_15==1] <- 0
table(clindata$cm2_39_notsma, useNA="ifany")
#check
table(clindata$cm2_39, clindata$sma5_15, useNA="ifany")


#---------------------------------------------------------------------------------------------------------------------
#*sma NOT cm1
#---------------------------------------------------------------------------------------------------------------------
clindata$smanotcm1_39 <-NA
#code
clindata$smanotcm1_39[clindata$cm1_39==0 & clindata$sma5_15==1] <- 1
clindata$smanotcm1_39[clindata$cm1_39==1 | clindata$sma5_15==0] <- 0
table(clindata$smanotcm1_39, useNA="ifany")
#check
table(clindata$cm1_39, clindata$sma5_15, useNA="ifany")


#---------------------------------------------------------------------------------------------------------------------
#*sma NOT cm2
#---------------------------------------------------------------------------------------------------------------------
clindata$smanotcm2 <-NA
#code
clindata$smanotcm2_39[clindata$cm2_39==0 & clindata$sma5_15==1] <- 1
clindata$smanotcm2_39[clindata$cm2_39==1 | clindata$sma5_15==0] <- 0
table(clindata$smanotcm2_39, useNA="ifany")
#check
table(clindata$cm2_39, clindata$sma5_15, useNA="ifany")


#---------------------------------------------------------------------------------------------------------------------
#*cm1 AND sma
#---------------------------------------------------------------------------------------------------------------------
clindata$cm1_39_andsma <-NA
#code
clindata$cm1_39_andsma[clindata$cm1_39==1 & clindata$sma5_15==1] <- 1
clindata$cm1_39_andsma[clindata$cm1_39==0 | clindata$sma5_15==0] <- 0
table(clindata$cm1_39_andsma, useNA="ifany")
#check
table(clindata$cm1_39, clindata$sma5_15, useNA="ifany")

#---------------------------------------------------------------------------------------------------------------------
#*cm2 AND sma
#---------------------------------------------------------------------------------------------------------------------
clindata$cm2_39_andsma <-NA
#code
clindata$cm2_39_andsma[clindata$cm2_39==1 & clindata$sma5_15==1] <- 1
clindata$cm2_39_andsma[clindata$cm2_39==0 | clindata$sma5_15==0] <- 0
table(clindata$cm2_39_andsma, useNA="ifany")
#check
table(clindata$cm2_39, clindata$sma5_15, useNA="ifany")

#---------------------------------------------------------------------------------------------------------------------
#** cm1orsma_control
#---------------------------------------------------------------------------------------------------------------------
#The below counts ONLY controls (ie controls from the strict_caseorcontrol variable whereas the 'cm1orsma' variable
#includes all of the cases that are NOT cm1 or sma. The same goes for the other phenotypes labeled similarly. 
clindata$cm1_39_orsma_control <-NA
#code
clindata$cm1_39_orsma_control[clindata$cm1_39_orsma==1] <- 1
clindata$cm1_39_orsma_control[clindata$strict_caseorcontrol==0] <- 0
table(clindata$cm1_39_orsma_control, useNA="ifany")
table(clindata$cm1_39_orsma, useNA="ifany")

#---------------------------------------------------------------------------------------------------------------------
#** cm2orsma_control
#---------------------------------------------------------------------------------------------------------------------
clindata$cm2_39_orsma_control <-NA
#code
clindata$cm2_39_orsma_control[clindata$cm2_39_orsma==1] <- 1
clindata$cm2_39_orsma_control[clindata$strict_caseorcontrol==0] <- 0
table(clindata$cm2_39_orsma_control, useNA="ifany")
table(clindata$cm2_39_orsma, useNA="ifany")

#---------------------------------------------------------------------------------------------------------------------
#** cm1notsma_control
#---------------------------------------------------------------------------------------------------------------------
clindata$cm1_39_notsma_control <-NA
#code
clindata$cm1_39_notsma_control[clindata$cm1_39_notsma==1] <- 1
clindata$cm1_39_notsma_control[clindata$strict_caseorcontrol==0] <- 0
table(clindata$cm1_39_notsma_control, useNA="ifany")
table(clindata$cm1_39_notsma, useNA="ifany")

#---------------------------------------------------------------------------------------------------------------------
#** cm2notsma_control
#---------------------------------------------------------------------------------------------------------------------
clindata$cm2_39_notsma_control <-NA
#code
clindata$cm2_39_notsma_control[clindata$cm2_39_notsma==1] <- 1
clindata$cm2_39_notsma_control[clindata$strict_caseorcontrol==0] <- 0
table(clindata$cm2_39_notsma_control, useNA="ifany")
table(clindata$cm2_39_notsma, useNA="ifany")


#---------------------------------------------------------------------------------------------------------------------
#** smanotcm1_control
#---------------------------------------------------------------------------------------------------------------------
clindata$smanotcm1_39_control <-NA
#code
clindata$smanotcm1_39_control[clindata$smanotcm1_39==1] <- 1
clindata$smanotcm1_39_control[clindata$strict_caseorcontrol==0] <- 0
table(clindata$smanotcm1_39_control, useNA="ifany")
table(clindata$smanotcm1_39, useNA="ifany")

#---------------------------------------------------------------------------------------------------------------------
#** smanotcm2_control
#---------------------------------------------------------------------------------------------------------------------
clindata$smanotcm2_39_control <-NA
#code
clindata$smanotcm2_39_control[clindata$smanotcm2_39==1] <- 1
clindata$smanotcm2_39_control[clindata$strict_caseorcontrol==0] <- 0
table(clindata$smanotcm2_39_control, useNA="ifany")
table(clindata$smanotcm2_39, useNA="ifany")

#---------------------------------------------------------------------------------------------------------------------
#** cm1andsma_control
#---------------------------------------------------------------------------------------------------------------------
clindata$cm1_39_andsma_control <-NA
#code
clindata$cm1_39_andsma_control[clindata$cm1_39_andsma==1] <- 1
clindata$cm1_39_andsma_control[clindata$strict_caseorcontrol==0] <- 0
table(clindata$cm1_39_andsma_control, useNA="ifany")
table(clindata$cm1_39_andsma, useNA="ifany")

#---------------------------------------------------------------------------------------------------------------------
#** cm2andsma_control
#---------------------------------------------------------------------------------------------------------------------
clindata$cm2_39_andsma_control <-NA
#code
clindata$cm2_39_andsma_control[clindata$cm2_39_andsma==1] <- 1
clindata$cm2_39_andsma_control[clindata$strict_caseorcontrol==0] <- 0
table(clindata$cm2_39_andsma_control, useNA="ifany")
table(clindata$cm2_39_andsma, useNA="ifany")


#----------------------------------------
#Strict cases not cm1_39, not sma 
#--------------------------------------------
clindata$casenotcm1_39notsma <-NA
#code
clindata$casenotcm1_39notsma[clindata$cm1_39==0 & clindata$sma5_15==0 ] <- 1
clindata$casenotcm1_39notsma[clindata$cm1_39==1 | clindata$sma5_15==1 ] <- 0
table(clindata$casenotcm1_39notsma, useNA="ifany")
                                        
#----------------------------------------
#Strict cases not cm1_39, not sma where 0=strict controls
#--------------------------------------------
clindata$casenotcm1_39notsma_control <-NA
#code
clindata$casenotcm1_39notsma_control[clindata$casenotcm1_39notsma ==1] <- 1
clindata$casenotcm1_39notsma_control[clindata$strict_caseorcontrol==0] <- 0
table(clindata$casenotcm1_39notsma_control, useNA="ifany")

###########################################################################################

### Append original study_code and sample_label and recreate sc_sl from curated versions
clindata$curated_sample_label <- clindata$sample_label
clindata$curated_study_code   <- clindata$study_code

clindata$original_sample_label <- clindata.ori$sample_label
clindata$original_study_code   <- clindata.ori$study_code

clindata$sc_sl <- paste(clindata$curated_study_code, "_", clindata$curated_sample_label, sep="")

#checks
cbind(as.character(clindata$curated_sample_label), as.character(clindata$original_sample_label))
table(clindata$curated_study_code, clindata$original_study_code)

clindata <- clindata[-which(names(clindata) == "sample_label")]
clindata <- clindata[-which(names(clindata) == "study_code")]

clindata$curated_gender <- NA  
clindata$gender_discrepencies <- NA

##########################################################################################
#########################################################################################

#create a dataframe to store data dictionary information
datdic <- data.frame( id_number = c(1:ncol(clindata)), name = names(clindata), details = NA, classification = NA)  #col 1 => columns number, col 2 => name, col3 => details


#datdic[which(names(clindata) == "row"), 1:4] <- c(which(names(clindata) == "row"), "row", "Row number")
datdic[which(names(clindata) == "id"), 1:4] <- c(which(names(clindata) == "id"), "id", "ID number", "housekeeping")
datdic[which(names(clindata) == "oldcaseorcontrol"), 1:4] <- c(which(names(clindata) == "oldcaseorcontrol"), "oldcaseorcontrol", "Reported cases and controls before accounting for reduction in Kenyan samples", "original_sampling")
datdic[which(names(clindata) == "estimated_age_months"), 1:4] <- c(which(names(clindata) == "estimated_age_months"), "estimated_age_months", "Estimated age in months", "original_sampling")
datdic[which(names(clindata) == "ethnic_group_father"), 1:4] <- c(which(names(clindata) == "ethnic_group_father"), "ethnic_group_father", "Ethnic group of father", "original_timeless")
datdic[which(names(clindata) == "ethnic_group_mother"), 1:4] <- c(which(names(clindata) == "ethnic_group_mother"), "ethnic_group_mother", "Ethnic group of mother", "original_timeless")
datdic[which(names(clindata) == "location_region"), 1:4] <- c(which(names(clindata) == "location_region"), "location_region", "Region in which the individual lives", "original_timeless")
datdic[which(names(clindata) == "clinical_gender"), 1:4] <- c(which(names(clindata) == "clinical_gender"), "clinical_gender", "Gender recorded when clinical data was collected in the field: F=female; M=male", "original_timeless")
datdic[which(names(clindata) == "sc_sl"), 1:4] <- c(which(names(clindata) == "sc_sl"), "sc_sl", "study code_sample label: : this is a unique identifier for each individual", "housekeeping")
datdic[which(names(clindata) == "location_village"), 1:4] <- c(which(names(clindata) == "location_village"), "location_village", "Village in which the individual lives", "original_timeless")
datdic[which(names(clindata) == "relationship_to_case"), 1:4] <- c(which(names(clindata) == "relationship_to_case"), "relationship_to_case", "What is the relationship to the case?", "housekeeping")
datdic[which(names(clindata) == "administered_anti_convulsants_after_admission"), 1:4] <- c(which(names(clindata) == "administered_anti_convulsants_after_admission"), "administered_anti_convulsants_after_admission", "Were anti convulsants administered after admission: yes/no", "original_sampling")
datdic[which(names(clindata) == "administered_glucose"), 1:4] <- c(which(names(clindata) == "administered_glucose"), "administered_glucose", "Was glucose administered: yes/no", "original_sampling")
datdic[which(names(clindata) == "admission_date"), 1:4] <- c(which(names(clindata) == "admission_date"), "admission_date", "Admission date to the hospital/clinic", "original_sampling")
datdic[which(names(clindata) == "admission_month"), 1:4] <- c(which(names(clindata) == "admission_month"), "admission_month", "Month of admission to the hospital/clinic", "derived")
datdic[which(names(clindata) == "admission_year"), 1:4] <- c(which(names(clindata) == "admission_year"), "admission_year", "Year of admission to the hospital/clinic", "derived")
datdic[which(names(clindata) == "admission_month_year"), 1:4] <- c(which(names(clindata) == "admission_month_year"), "admission_month_year", "Month & year of admission to the hospital/clinic", "derived")
datdic[which(names(clindata) == "sample_date"), 1:4] <- c(which(names(clindata) == "sample_date"), "sample_date", "Date sample taken", "original_sampling")
datdic[which(names(clindata) == "bcs_assessed30_min_after_convulsions_blood_sugar_2_1mm_l"), 1:4] <- c(which(names(clindata) == "bcs_assessed30_min_after_convulsions_blood_sugar_2_1mm_l"), "bcs_assessed30_min_after_convulsions_blood_sugar_2_1mm_l", "Was BCS assessed at least 30 min. after the last convulsion and when blood sugar was higher than 2.1 mm/l: yes/no", "original_sampling")
datdic[which(names(clindata) == "birth_date"), 1:4] <- c(which(names(clindata) == "birth_date"), "birth_date", "Birth date", "original_timeless")
datdic[which(names(clindata) == "blantyre_coma_score"), 1:4] <- c(which(names(clindata) == "blantyre_coma_score"), "blantyre_coma_score", "Blantyre Coma Score", "original_sampling")
datdic[which(names(clindata) == "blantyre_eye_score"), 1:4] <- c(which(names(clindata) == "blantyre_eye_score"), "blantyre_eye_score", "Blantyre Eye Score", "original_sampling")
datdic[which(names(clindata) == "blantyre_motor_score"), 1:4] <- c(which(names(clindata) == "blantyre_motor_score"), "blantyre_motor_score", "Blantyre Motor Score", "original_sampling")
datdic[which(names(clindata) == "blantyre_verbal_score"), 1:4] <- c(which(names(clindata) == "blantyre_verbal_score"), "blantyre_verbal_score", "Blantyre Verbal Score", "original_sampling")
datdic[which(names(clindata) == "blood_transfusion_received"), 1:4] <- c(which(names(clindata) == "blood_transfusion_received"), "blood_transfusion_received", "Was a blood_transfusion_received: yes/no", "original_sampling")
datdic[which(names(clindata) == "convulsions_after_admission"), 1:4] <- c(which(names(clindata) == "convulsions_after_admission"), "convulsions_after_admission", "Did the indidual have convulsions_after_admission: yes/no", "original_sampling")
datdic[which(names(clindata) == "convulsions_within24_hours_prior_to_admission"), 1:4] <- c(which(names(clindata) == "convulsions_within24_hours_prior_to_admission"), "convulsions_within24_hours_prior_to_admission", "Did the indidual have convulsions_within24_hours_prior_to_admission: yes/no", "original_sampling")
datdic[which(names(clindata) == "date_death_discharge_absconded"), 1:4] <- c(which(names(clindata) == "date_death_discharge_absconded"), "date_death_discharge_absconded", "Date individual died or was discharged or absconded", "original_sampling")
datdic[which(names(clindata) == "dehydrated"), 1:4] <- c(which(names(clindata) == "dehydrated"), "dehydrated", "Was the individual dehydrated? Yes/no", "original_sampling")
datdic[which(names(clindata) == "fever_within_past48_hours"), 1:4] <- c(which(names(clindata) == "fever_within_past48_hours"), "fever_within_past48_hours", "Did the individual have fever within past 48 hours: yes/no", "original_sampling")
datdic[which(names(clindata) == "fitting_now"), 1:4] <- c(which(names(clindata) == "fitting_now"), "fitting_now", "Is the child fitting now? Yes/no", "original_sampling")
datdic[which(names(clindata) == "glucose_mmol_l"), 1:4] <- c(which(names(clindata) == "glucose_mmol_l"), "glucose_mmol_l", "Glucose level (0.0-30.0 mmol/l)", "original_sampling")
datdic[which(names(clindata) == "haemoglobin_g_dl"), 1:4] <- c(which(names(clindata) == "haemoglobin_g_dl"), "haemoglobin_g_dl", "Haemoglobin level (1.0-18.0 g/dl)", "original_sampling")
datdic[which(names(clindata) == "hematocrit"), 1:4] <- c(which(names(clindata) == "hematocrit"), "hematocrit", "Haematocrit level (5.0-45.0 %)", "original_sampling")
datdic[which(names(clindata) == "jaundice_now"), 1:4] <- c(which(names(clindata) == "jaundice_now"), "jaundice_now", "Jaundice? Yes/no", "original_sampling")
datdic[which(names(clindata) == "anaemia_status"), 1:4] <- c(which(names(clindata) == "anaemia_status"), "anaemia_status", "Anaemic? Yes/no", "original_sampling")
datdic[which(names(clindata) == "lactate_mmol_l"), 1:4] <- c(which(names(clindata) == "lactate_mmol_l"), "lactate_mmol_l", "Lactate level (0.0-20.0 mmol/l)", "original_sampling")
datdic[which(names(clindata) == "neck_stiffness_bulging_fontenelle"), 1:4] <- c(which(names(clindata) == "neck_stiffness_bulging_fontenelle"), "neck_stiffness_bulging_fontenelle", "Neck stiffness or bulging fontenelle? Yes/no", "original_sampling")
datdic[which(names(clindata) == "outcome"), 1:4] <- c(which(names(clindata) == "outcome"), "outcome", "Did the individual die? Survive? Abscond?", "original_sampling")
datdic[which(names(clindata) == "parasitemia_parasites_ul"), 1:4] <- c(which(names(clindata) == "parasitemia_parasites_ul"), "parasitemia_parasites_ul", "Parasitaemia (100-2500000 asexual parasite/�l blood)", "original_sampling")
datdic[which(names(clindata) == "respiratory_distress"), 1:4] <- c(which(names(clindata) == "respiratory_distress"), "respiratory_distress", "Was the individual in respiratory distress?", "original_sampling")
datdic[which(names(clindata) == "respiratory_rate_per_minute"), 1:4] <- c(which(names(clindata) == "respiratory_rate_per_minute"), "respiratory_rate_per_minute", "Respiratory rate per minute", "original_sampling")
datdic[which(names(clindata) == "spleen_enlarged_cm"), 1:4] <- c(which(names(clindata) == "spleen_enlarged_cm"), "spleen_enlarged_cm", "Size of enlarged spleen (cm)", "original_sampling")
datdic[which(names(clindata) == "temperature_c"), 1:4] <- c(which(names(clindata) == "temperature_c"), "temperature_c", "Temperature (degrees celcius)", "original_sampling")
datdic[which(names(clindata) == "weight_kg"), 1:4] <- c(which(names(clindata) == "weight_kg"), "weight_kg", "Weight in kg", "original_sampling")
datdic[which(names(clindata) == "height_cm"), 1:4] <- c(which(names(clindata) == "height_cm"), "height_cm", "Height in cm", "original_sampling")
datdic[which(names(clindata) == "antimalarials_past7_days"), 1:4] <- c(which(names(clindata) == "antimalarials_past7_days"), "antimalarials_past7_days", "Had the individual taken anti-malarials in the past 7 days?", "original_sampling")
datdic[which(names(clindata) == "administered_anti_malarials_prior_to_admission"), 1:4] <- c(which(names(clindata) == "administered_anti_malarials_prior_to_admission"), "administered_anti_malarials_prior_to_admission", "Was the individual given anti-malarials prior to admission", "original_sampling")
datdic[which(names(clindata) == "admission_date_minus_birth_date_months"), 1:4] <- c(which(names(clindata) == "admission_date_minus_birth_date_months"), "admission_date_minus_birth_date_months", "Admission date minus birth date months (gives the age of the child in months)", "original_sampling")
datdic[which(names(clindata) == "admission_time"), 1:4] <- c(which(names(clindata) == "admission_time"), "admission_time", "Admission time", "original_sampling")
datdic[which(names(clindata) == "child_can_sit_unaided_breastfeed"), 1:4] <- c(which(names(clindata) == "child_can_sit_unaided_breastfeed"), "child_can_sit_unaided_breastfeed", "Can the child sit unaided/able to breastfeed? Yes/no", "original_sampling")
datdic[which(names(clindata) == "malaria_positive"), 1:4] <- c(which(names(clindata) == "malaria_positive"), "malaria_positive", "Is the individual malaria_positive", "original_sampling")
datdic[which(names(clindata) == "matched_father"), 1:4] <- c(which(names(clindata) == "matched_father"), "matched_father", "Sample label of matched father", "housekeeping")
datdic[which(names(clindata) == "matched_mother"), 1:4] <- c(which(names(clindata) == "matched_mother"), "matched_mother", "Sample label of matched mother", "housekeeping")
datdic[which(names(clindata) == "temperature_site"), 1:4] <- c(which(names(clindata) == "temperature_site"), "temperature_site", "Site at which temperature was taken", "original_sampling")
datdic[which(names(clindata) == "matched_individual"), 1:4] <- c(which(names(clindata) == "matched_individual"), "matched_individual", "Sample label of matched individual", "housekeeping")
datdic[which(names(clindata) == "relationship_to_control"), 1:4] <- c(which(names(clindata) == "relationship_to_control"), "relationship_to_control", "What is the relationship_to the control?", "housekeeping")
datdic[which(names(clindata) == "bipedal_odema"), 1:4] <- c(which(names(clindata) == "bipedal_odema"), "bipedal_odema", "Does the individual have bipedal odema? Yes/no", "original_sampling")
datdic[which(names(clindata) == "consent_obtained"), 1:4] <- c(which(names(clindata) == "consent_obtained"), "consent_obtained", "Was consent obtained? Yes/no", "original_sampling")
datdic[which(names(clindata) == "generalised_lymphadenopathy_oral_candiasis"), 1:4] <- c(which(names(clindata) == "generalised_lymphadenopathy_oral_candiasis"), "generalised_lymphadenopathy_oral_candiasis", "Does the individual have Generalised lymphadenopathy / oral candidiasis? Yes/no", "original_sampling")
datdic[which(names(clindata) == "other_diagnosis_other"), 1:4] <- c(which(names(clindata) == "other_diagnosis_other"), "other_diagnosis_other", "Did the individual have another diagnosis other than those also listed below", "original_sampling")
datdic[which(names(clindata) == "capillary_refill_2seconds"), 1:4] <- c(which(names(clindata) == "capillary_refill_2seconds"), "capillary_refill_2seconds", "Capillary refill time >2 seconds: yes/no", "original_sampling")
datdic[which(names(clindata) == "administered_glucose_coma_resolved"), 1:4] <- c(which(names(clindata) == "administered_glucose_coma_resolved"), "administered_glucose_coma_resolved", "Did the coma resolve after administration of glucose: yes/no", "original_sampling")
datdic[which(names(clindata) == "mid_upper_arm_cirumference_muac_cm"), 1:4] <- c(which(names(clindata) == "mid_upper_arm_cirumference_muac_cm"), "mid_upper_arm_cirumference_muac_cm", "Mid upper arm cirumference (muac) in cm", "original_sampling")
datdic[which(names(clindata) == "other_diagnosis_gastroenteritis"), 1:4] <- c(which(names(clindata) == "other_diagnosis_gastroenteritis"), "other_diagnosis_gastroenteritis", "Was the individual diagnosed with gastroenteritis: yes/no", "original_sampling")
datdic[which(names(clindata) == "other_diagnosis_malnutrition"), 1:4] <- c(which(names(clindata) == "other_diagnosis_malnutrition"), "other_diagnosis_malnutrition", "Was the individual diagnosed with malnutrition: yes/no", "original_sampling")
datdic[which(names(clindata) == "other_diagnosis_meningitis"), 1:4] <- c(which(names(clindata) == "other_diagnosis_meningitis"), "other_diagnosis_meningitis", "Was the individual diagnosed with meningitis: yes/no", "original_sampling")
datdic[which(names(clindata) == "other_diagnosis_respiratory_infection"), 1:4] <- c(which(names(clindata) == "other_diagnosis_respiratory_infection"), "other_diagnosis_respiratory_infection", "Was the individual diagnosed with respiratory infection: yes/no", "original_sampling")
datdic[which(names(clindata) == "other_diagnosis_sickle_cell"), 1:4] <- c(which(names(clindata) == "other_diagnosis_sickle_cell"), "other_diagnosis_sickle_cell", "Was the individual diagnosed with sickle cell: yes/no", "original_sampling")
datdic[which(names(clindata) == "time_death"), 1:4] <- c(which(names(clindata) == "time_death"), "time_death", "Time of death", "original_sampling")
datdic[which(names(clindata) == "other_diagnosis_unknown"), 1:4] <- c(which(names(clindata) == "other_diagnosis_unknown"), "other_diagnosis_unknown", "Did the individual have an unknown diagnosis: yes/no", "original_sampling")
datdic[which(names(clindata) == "do_not_use"), 1:4] <- c(which(names(clindata) == "do_not_use"), "do_not_use", "These records do not have sufficient information to include in analysis", "original_sampling")
datdic[which(names(clindata) == "non_falciparum_parasitemia_parasites_ul"), 1:4] <- c(which(names(clindata) == "non_falciparum_parasitemia_parasites_ul"), "non_falciparum_parasitemia_parasites_ul", "Non falciparum parasitemia (ul)", "original_sampling")
datdic[which(names(clindata) == "falciparum_parasitemia_parasites_�l"), 1:4] <- c(which(names(clindata) == "falciparum_parasitemia_parasites_�l"), "falciparum_parasitemia_parasites_�l", "Falciparum parasitemia (ul)", "original_sampling")
datdic[which(names(clindata) == "curated_falciparum_parasitemia"), 1:4] <- c(which(names(clindata) == "curated_falciparum_parasitemia"), "curated_falciparum_parasitemia", "Falciparum parasitemia (ul) combining parasitemia & falciparum parasitemia fields", "original_sampling")
datdic[which(names(clindata) == "spleen_enlarged_yes_no"), 1:4] <- c(which(names(clindata) == "spleen_enlarged_yes_no"), "spleen_enlarged_yes_no", "Was the spleen enlarged? Yes/no", "original_sampling")
datdic[which(names(clindata) == "glasgow_coma_score"), 1:4] <- c(which(names(clindata) == "glasgow_coma_score"), "glasgow_coma_score", "Glasgow coma score", "original_sampling")
datdic[which(names(clindata) == "glasgow_eye_response"), 1:4] <- c(which(names(clindata) == "glasgow_eye_response"), "glasgow_eye_response", "Glasgow eye response", "original_sampling")
datdic[which(names(clindata) == "glasgow_motor_response"), 1:4] <- c(which(names(clindata) == "glasgow_motor_response"), "glasgow_motor_response", "Glasgow motor response", "original_sampling")
datdic[which(names(clindata) == "glasgow_verbal_response"), 1:4] <- c(which(names(clindata) == "glasgow_verbal_response"), "glasgow_verbal_response", "Glasgow verbal response", "original_sampling")
datdic[which(names(clindata) == "cerebral_malaria_status"), 1:4] <- c(which(names(clindata) == "cerebral_malaria_status"), "cerebral_malaria_status", "Cerebral malaria status: Yes= has malaria; no=no malaria", "original_sampling")
datdic[which(names(clindata) == "curated_sample_label"), 1:4] <- c(which(names(clindata) == "curated_sample_label"), "curated_sample_label", "Reformatted source_code/ label to match genotyped sample IDs", "housekeeping")
datdic[which(names(clindata) == "curated_study_code"), 1:4] <- c(which(names(clindata) == "curated_study_code"), "curated_study_code", "Reformatted codes given to each study: BD: Burkina Faso; BE: Burkina Faso; DG: Mali; GA: Gambia; GB: Gambia; GF: Gambia; GJ: Gambia; GM: Gambia; GU: Gambia;MF:Malawi; MU:Malawi; NA: PNG;   NB: PNG; NC: PNG; NG: PNG; NH: PNG; NJ: PNG; NS:Nigeria; NT:Nigeria; TJ: Tanzania; TK:Tanzania; VB: Vietnam; VC: Vietnam; VF: Vietnam; VM: Vietnam; VU: Vietnam; ZB: Cameroon;  ZG:Ghana;  ZK:Kenya ", "housekeeping")
datdic[which(names(clindata) == "original_sample_label"), 1:4] <- c(which(names(clindata) == "original_sample_label"), "original_sample_label", "Source_code/ label provided by site", "housekeeping")
datdic[which(names(clindata) == "original_study_code"), 1:4] <- c(which(names(clindata) == "original_study_code"), "original_study_code", "Code given to each study in ToPheno", "housekeeping")
datdic[which(names(clindata) == "hypoglycaemia"), 1:4] <- c(which(names(clindata) == "hypoglycaemia"), "hypoglycaemia", "Is the individual hypoglycemic: yes/no (this relates to Kenya data that has no record of levels of hypoglc only this variable)", "original_sampling")
datdic[which(names(clindata) == "caseorcontrol" ), 1:4] <- c(which(names(clindata) == "caseorcontrol" ), "caseorcontrol" ,"1 = CASE; 2 = CONTROL accounting for reduction in Kenyan samples", "original_sampling" )
datdic[which(names(clindata) == "country_code" ), 1:4] <- c(which(names(clindata) == "country_code" ), "country_code" ,"ISO 2-letter code for country", "housekeeping" )
datdic[which(names(clindata) == "country_name" ), 1:4] <- c(which(names(clindata) == "country_name" ), "country_name" ,"Name of country", "housekeeping" )
datdic[which(names(clindata) == "country_order" ), 1:4] <- c(which(names(clindata) == "country_order" ), "country_order","Sorting order for countries in MalariaGEN -> East to West", "housekeeping" )
datdic[which(names(clindata) == "site_name" ), 1:4] <- c(which(names(clindata) == "site_name" ), "site_name" ,"MalariaGEN study sites", "housekeeping" )
datdic[which(names(clindata) == "site_order" ), 1:4] <- c(which(names(clindata) == "site_order" ), "site_order","Sort order for MalariaGEN study_sites -> East to West", "housekeeping" )
datdic[which(names(clindata) == "country_site" ), 1:4] <- c(which(names(clindata) == "country_site" ),"country_site", "This variable names the countries and splits Ghana by site", "housekeeping")
datdic[which(names(clindata) == "curated_ethnicity" ), 1:4] <- c(which(names(clindata) == "curated_ethnicity" ), "curated_ethnicity","comparison & merger of mother and father ethnic groups", "derived" )
datdic[which(names(clindata) == "country_ethnicity" ), 1:4] <- c(which(names(clindata) == "country_ethnicity" ), "country_ethnicity", "Curated ethnicity with the country_site pasted to the front; it has not been grouped into 'other' for those ethnicities with low proportions", "derived")
datdic[which(names(clindata) == "ce_sort_order" ), 1:4] <- c(which(names(clindata) == "ce_sort_order" ), "ce_sort_order","Sort order of country_ethnicity", "housekeeping")
datdic[which(names(clindata) == "new_country_ethnicity" ), 1:4] <- c(which(names(clindata) == "new_country_ethnicity" ), "new_country_ethnicity","Ethnicities are linked to the country name and any ethnicity with a proportion <0.05 in the country is recoded to country_other", "derived")
datdic[which(names(clindata) == "age_months" ), 1:4] <- c(which(names(clindata) == "age_months" ), "age_months","Values represent months" , "derived" )
datdic[which(names(clindata) == "agegp5" ), 1:4] <- c(which(names(clindata) == "agegp5" ), "agegp5","Ages grouped into 5 categories", "derived")
datdic[which(names(clindata) == "agegp6" ), 1:4] <- c(which(names(clindata) == "agegp6" ), "agegp6","Ages grouped into 6 categories", "derived")
datdic[which(names(clindata) == "pf_parasites" ), 1:4] <- c(which(names(clindata) == "pf_parasites" ), "pf_parasites","Presence/absence of P.falciparum parasites:  0=NO parasites; 1=YES parasites", "derived")
datdic[which(names(clindata) == "nonpf_parasites" ), 1:4] <- c(which(names(clindata) == "nonpf_parasites" ), "nonpf_parasites","Presence/absence of non-P.falciparum parasites:  0=NO parasites; 1=YES parasites", "derived")
datdic[which(names(clindata) == "logpospara" ), 1:4] <- c(which(names(clindata) == "logpospara" ), "logpospara","Log10 parasitemia with zero parasitemias recoded as NA", "derived")
datdic[which(names(clindata) == "logallpara" ), 1:4] <- c(which(names(clindata) == "logallpara" ), "logallpara","Log10 parasitemia with zero parasitemias recoded as 0", "derived")
datdic[which(names(clindata) == "other_diagnosis" ), 1:4] <- c(which(names(clindata) == "other_diagnosis" ), "other_diagnosis","other_diagnosis from malaria:  0=NO; 1=YES", "derived")
datdic[which(names(clindata) == "other_diagnosis_names" ), 1:4] <- c(which(names(clindata) == "other_diagnosis_names" ), "other_diagnosis_names","other_diagnosis from malaria as name", "derived")
datdic[which(names(clindata) == "other_diagnosis_use" ), 1:4] <- c(which(names(clindata) == "other_diagnosis_use" ), "other_diagnosis_use","other_diagnosis to use in malaria:  0=NO; 1=YES", "derived")
datdic[which(names(clindata) == "cleaned_case" ), 1:4] <- c(which(names(clindata) == "cleaned_case" ), "cleaned_case"," A CASE with parasites:  0=NO; 1=YES", "derived")
datdic[which(names(clindata) == "cleaned_control" ), 1:4] <- c(which(names(clindata) == "cleaned_control" ), "cleaned_control"," A CONTROL without parasites:  0=NO; 1=YES", "derived")
datdic[which(names(clindata) == "hyper_parasit10000" ), 1:4] <- c(which(names(clindata) == "hyper_parasit10000" ), "hyper_parasit10000","P.f parasitemia >10000:  0=NO; 1=YES", "derived")
datdic[which(names(clindata) == "hyper_parasit100000" ), 1:4] <- c(which(names(clindata) == "hyper_parasit100000" ), "hyper_parasit100000","P.f parasitemia >100000:  0=NO; 1=YES", "derived")
datdic[which(names(clindata) == "hyper_parasit500000" ), 1:4] <- c(which(names(clindata) == "hyper_parasit500000" ), "hyper_parasit500000","P.f parasitemia >500000:  0=NO; 1=YES", "derived")
datdic[which(names(clindata) == "strict_caseorcontrol" ), 1:4] <- c(which(names(clindata) == "strict_caseorcontrol" ), "strict_caseorcontrol","Malaria case P.f parasite positive only  or controls that are Pf or non-P.f parasite negative:  0=control; 1=case", "derived")
datdic[which(names(clindata) == "strict_caseorcontrolparents" ), 1:4] <- c(which(names(clindata) == "strict_caseorcontrolparents" ), "strict_caseorcontrolparents","Malaria case parasite positive only  or controls that are parasite negative or parents", "derived")
datdic[which(names(clindata) == "hypog39" ), 1:4] <- c(which(names(clindata) == "hypog39" ), "hypog39", "Hypoglycaemic (glucose<3.9mM)  0=NO; 1=YES", "derived")
datdic[which(names(clindata) == "hypog21" ), 1:4] <- c(which(names(clindata) == "hypog21" ), "hypog21", "Hypoglycaemic (glucose<2.1mM)  0=NO; 1=YES", "derived")
datdic[which(names(clindata) == "bcs3" ), 1:4] <- c(which(names(clindata) == "bcs3" ),"bcs3", "Blantyre Coma Score <=3:  0=NO; 1=YES", "derived")
datdic[which(names(clindata) == "gcs9"), 1:4] <- c (which(names(clindata) == "gcs9"), "gcs9", "Glasgow Coma Score <=9: 0=NO; 1=YES", "derived")
datdic[which(names(clindata) == "bcs2" ), 1:4] <- c(which(names(clindata) == "bcs2" ),"bcs2", "Blantyre Coma Score <=2:  0=NO; 1=YES", "derived")
datdic[which(names(clindata) == "gcs8"), 1:4] <- c (which(names(clindata) == "gcs8"), "gcs8", "Glasgow Coma Score <=8: 0=NO; 1=YES", "derived")
datdic[which(names(clindata) == "coma_status_3_9"), 1:4] <- c (which(names(clindata) == "coma_status_3_9"), "coma_status_3_9", "BCS <=3 or GCS <=9: 0=NO; 1=YES", "derived")
datdic[which(names(clindata) == "coma_status_2_8"), 1:4] <- c (which(names(clindata) == "coma_status_2_8"), "coma_status_2_8", "BCS <=2 or GCS <=8: 0=NO; 1=YES", "derived")
datdic[which(names(clindata) == "hypoglyc_status"), 1:4] <- c (which(names(clindata) == "hypoglyc_status"), "hypoglyc_status", "NOT hypoglycemic at admission OR at assessment: 0=NO; 1=YES", "derived")
datdic[which(names(clindata) == "cm1_28"), 1:4] <- c (which(names(clindata) == "cm1_28"), "cm1_28", "Cerebral malaria 1_28: strict case with a coma (based on BCS<=2 and GCS<=8)includes DR kenya group: 0=Case without cm1 traits; 1=Case with cm1 traits", "derived")
datdic[which(names(clindata) == "cm1_39"), 1:4] <- c (which(names(clindata) == "cm1_39"), "cm1_39", "Cerebral malaria 1_39: strict case with a coma (based on BCS<=3 and GCS<=9): 0=Case without cm1 traits; 1=Case with cm1 traits", "derived")
datdic[which(names(clindata) == "cm2_28"), 1:4] <- c (which(names(clindata) == "cm2_28"), "cm2_28", "Cerebral malaria 2_28: strict case with a coma (based on BCS<=2 and GCS<=8)AND not hypoglycemic: 0=Case without cm2 traits; 1=Case with cm2 traits", "derived")
datdic[which(names(clindata) == "cm2_39"), 1:4] <- c (which(names(clindata) == "cm2_39"), "cm2_39", "Cerebral malaria 2_39: strict case with a coma (based on BCS<=2 and GCS<=8) AND not hypoglycemic: 0=Case without cm2 traits; 1=Case with cm2 traits", "derived")
datdic[which(names(clindata) == "cm1_28_control"), 1:4] <- c (which(names(clindata) == "cm1_28_control"), "cm1_28_control", "Cerebral malaria 1_28: strict case with a coma (based on BCS<=2 and GCS<=8): 0=Strict control; 1=Case with cm1 traits", "derived")
datdic[which(names(clindata) == "cm1_39_control"), 1:4] <- c (which(names(clindata) == "cm1_39_control"), "cm1_39_control", "Cerebral malaria 1_39 : strict case with a coma (based on BCS<=3 and GCS<=9): 0=Strict control; 1=Case with cm1 traits", "derived")
datdic[which(names(clindata) == "cm2_28_control"), 1:4] <- c (which(names(clindata) == "cm2_28_control"), "cm2_28_control", "Cerebral malaria 2_28: strict case with a coma (based on BCS<=2 and GCS<=8) AND not hypoglycemic: 0=Strict control; 1=Case with cm2 traits", "derived")
datdic[which(names(clindata) == "cm2_39_control"), 1:4] <- c (which(names(clindata) == "cm2_39_control"), "cm2_39_control", "Cerebral malaria 2_39 : strict case with a coma (based on BCS<=3 and GCS<=9) AND not hypoglycemic: 0=Strict control; 1=Case with cm2 traits", "derived")
datdic[which(names(clindata) == "haem_5"), 1:4] <- c (which(names(clindata) == "haem_5"), "haem_5", "Haemoglobin <5: 0=NO; 1=YES", "derived")
datdic[which(names(clindata) == "haem_6"), 1:4] <- c (which(names(clindata) == "haem_6"), "haem_6", "Haemoglobin <6: 0=NO; 1=YES", "derived")
datdic[which(names(clindata) == "hemat_15"), 1:4] <- c (which(names(clindata) == "hemat_15"), "hemat_15", "hematocrit <15: 0=NO; 1=YES", "derived")
datdic[which(names(clindata) == "hemat_18"), 1:4] <- c (which(names(clindata) == "hemat_18"), "hemat_18", "hematocrit <18: 0=NO; 1=YES", "derived")
datdic[which(names(clindata) == "haem5_hemat15"), 1:4] <- c (which(names(clindata) == "haem5_hemat15"), "haem5_hemat15", "Haemoglobin (<5) or hematocrit (<15) if Hb not available: 0=No; 1=Yes", "derived")
datdic[which(names(clindata) == "haem6_hemat18"), 1:4] <- c (which(names(clindata) == "haem6_hemat18"), "haem6_hemat18", "Haemoglobin (<6) or hematocrit (<18) if Hb not available: 0=No; 1=Yes", "derived")
datdic[which(names(clindata) == "curated_hb"), 1:4] <- c (which(names(clindata) == "curated_hb"), "curated_hb", "Haemoglobin calculated as haematocrit divided by 3 for sites that only provided haematocrit")
datdic[which(names(clindata) == "sma5_15"), 1:4] <- c (which(names(clindata) == "sma5_15"), "sma5_15", "Strict case and haem<5 or haematocrit<15: 0=cases without trait; 1=cases with trait", "derived")
datdic[which(names(clindata) == "sma6_18"), 1:4] <- c (which(names(clindata) == "sma6_18"), "sma6_18", "Strict case and haem<6 or haematocrit<18: 0=Cases without trait; 1=cases with trait", "derived")
datdic[which(names(clindata) == "sma5_15_control"), 1:4] <- c (which(names(clindata) == "sma5_15_control"), "sma5_15_control", "Strict case and haem<5 or haematocrit<15: 0=Strict control; 1=Case with trait", "derived")
datdic[which(names(clindata) == "sma6_18_control"), 1:4] <- c (which(names(clindata) == "sma6_18_control"), "sma6_18_control", "Strict case and haem<6 or haematocrit<18: 0=Strict control; 1=Case with trait", "derived")
datdic[which(names(clindata) == "died"), 1:4] <- c (which(names(clindata) == "died"), "died", "Child died?: 0=NO; 1=YES", "derived")
datdic[which(names(clindata) == "died_case"), 1:4] <- c (which(names(clindata) == "died_case"), "died_case", "Strict case that died: 0=strict case didn't die; 1=Strict case that died", "derived")
datdic[which(names(clindata) == "died_case_control"), 1:4] <- c (which(names(clindata) == "died_case_control"), "died_case_control", "Strict case that died: 0=Strict control; 1=Strict case that died", "derived")
datdic[which(names(clindata) == "cm1orsma"), 1:4] <- c (which(names(clindata) == "cm1orsma"), "cm1orsma", "Cerebral malaria (bcs<2 or gcs<8 and parasites) OR severe anaemia malaria (haemoglobin< or hematocrit<15 and parasites): 0=Case without trait; 1=Case with trait", "derived")
datdic[which(names(clindata) == "cm2orsma"), 1:4] <- c (which(names(clindata) == "cm2orsma"), "cm2orsma", "Cerebral malaria2 OR severe anaemia malaria (haemoglobin< or hematocrit<15 and parasites) and NOT hypoglycemic:  0=Case without trait; 1=Case with trait", "derived")
datdic[which(names(clindata) == "cm1notsma"), 1:4] <- c (which(names(clindata) == "cm1notsma"), "cm1notsma", "Cerebral malaria1 NOT severe anaemia malaria:  0=Case without trait; 1=Case with trait", "derived")
datdic[which(names(clindata) == "cm2notsma"), 1:4] <- c (which(names(clindata) == "cm2notsma"), "cm2notsma", "Cerebral malaria2 NOT severe anaemia malaria: 0=Case without trait; 1=Case with trait", "derived")
datdic[which(names(clindata) == "smanotcm1"), 1:4] <- c (which(names(clindata) == "smanotcm1"), "smanotcm1", "Severe anaemia malaria NOT Cerebral malaria1: 0=Case without trait; 1=Case with trait", "derived")
datdic[which(names(clindata) == "smanotcm2"), 1:4] <- c (which(names(clindata) == "smanotcm2"), "smanotcm2", "Severe anaemia malaria NOT Cerebral malaria2: 0=Case without trait; 1=Case with trait", "derived")
datdic[which(names(clindata) == "cm1andsma"), 1:4] <- c (which(names(clindata) == "cm1andsma"), "cm1andsma", "Severe anaemia malaria AND Cerebral malaria1: 0=Case without trait; 1=Case with trait", "derived")
datdic[which(names(clindata) == "cm2andsma"), 1:4] <- c (which(names(clindata) == "cm2andsma"), "cm2andsma", "Severe anaemia malaria AND Cerebral malaria1: 0=Case without trait; 1=Case with trait", "derived")
datdic[which(names(clindata) == "cm1orsma_control"), 1:4] <- c (which(names(clindata) == "cm1orsma_control"), "cm1orsma_control", "cerebral malaria1 OR severe anaemia malaria: 0=strict control; 1=Case with trait", "derived")
datdic[which(names(clindata) == "cm2orsma_control"), 1:4] <- c (which(names(clindata) == "cm2orsma_control"), "cm2orsma_control", "cerebral malaria2 OR severe anaemia malaria: 0=strict control; 1=Case with trait", "derived")
datdic[which(names(clindata) == "cm1notsma_control"), 1:4] <- c (which(names(clindata) == "cm1notsma_control"), "cm1notsma_control", "cerebral malaria1 NOT severe anaemia malaria: 0=strict control; 1=Case with trait", "derived")
datdic[which(names(clindata) == "cm2notsma_control"), 1:4] <- c (which(names(clindata) == "cm2notsma_control"), "cm2notsma_control", "cerebral malaria2 NOT severe anaemia malaria: 0=strict control; 1=Case with trait", "derived")
datdic[which(names(clindata) == "smanotcm1_control"), 1:4] <- c (which(names(clindata) == "smanotcm1_control"), "smanotcm1_control", "Severe anaemia malaria NOT cerebral malaria1: 0=strict control; 1=Case with trait", "derived")
datdic[which(names(clindata) == "smanotcm2_control"), 1:4] <- c (which(names(clindata) == "smanotcm2_control"), "smanotcm2_control", "Severe anaemia malaria NOT cerebral malaria2: 0=strict control; 1=Case with trait", "derived")
datdic[which(names(clindata) == "cm1andsma_control"), 1:4] <- c (which(names(clindata) == "cm1andsma_control"), "cm1andsma_control", "cerebral malaria1 AND severe anaemia malaria: 0=strict control; 1=Case with trait", "derived")
datdic[which(names(clindata) == "cm2andsma_control"), 1:4] <- c (which(names(clindata) == "cm2andsma_control"), "cm2andsma_control", "cerebral malaria2 AND severe anaemia malaria: 0=strict control; 1=Case with trait", "derived")
datdic[which(names(clindata) == "casenotcm1notsma"), 1:4] <- c (which(names(clindata) == "casenotcm1notsma"), "casenotcm1notsma", "Strict case without cm1_28 or sma: 0=Case with phenotypes; 1=Severe case without phenotypes", "derived")
datdic[which(names(clindata) == "casenotcm1notsma_control"), 1:4] <- c (which(names(clindata) == "casenotcm1notsma_control"), "casenotcm1notsma", "Strict case without cm1_28 or sma: 0=Strict control; 1=Severe case without phenoytpes", "derived")
datdic[which(names(clindata) == "died_cm1_28"), 1:4] <- c (which(names(clindata) == "died_cm1_28"), "died_cm1_28", "Died: 0=cm1_28 positive didn't die; 1=cm1_28 positive that died", "derived")
datdic[which(names(clindata) == "died_cm2_28"), 1:4] <- c (which(names(clindata) == "died_cm2_28"), "died_cm2_28", "Died: 0=cm2_28 positive didn't die; 1=cm2_28 positive that died", "derived")
datdic[which(names(clindata) == "died_sma_5_15"), 1:4] <- c (which(names(clindata) == "died_sma_5_15"), "died_sma_5_15", "Died: 0=sma_5_15 positive didn't die; 1=sma_5_15 positive that died", "derived")
datdic[which(names(clindata) == "died_control"), 1:4] <- c (which(names(clindata) == "died_control"), "died_control", "Child died: 0=control; 1=child died", "derived")
datdic[which(names(clindata) == "died_cm1_28_control"), 1:4] <- c (which(names(clindata) == "died_cm1_28_control"), "died_cm1_28_control", "Died: 0=strict control; 1=cm1_28 positive that died", "derived")
datdic[which(names(clindata) == "died_cm2_28_control"), 1:4] <- c (which(names(clindata) == "died_cm2_28_control"), "died_cm2_28_control", "Died: 0=strict control; 1=cm2_28 positive that died", "derived")
datdic[which(names(clindata) == "died_sma5_15_control"), 1:4] <- c (which(names(clindata) == "died_sma5_15_control"), "died_sma5_15_control", "Died: 0=strict control; 1=sma5_15 positive that died", "derived")
datdic[which(names(clindata) == "diedcm1orsma_control"), 1:4] <- c (which(names(clindata) == "diedcm1orsma_control"), "diedcm1orsma_control", "Child died and has cerebral malaria1 or severe anemaia: 0=strict control; 1=YES", "derived")
datdic[which(names(clindata) == "diedcm2orsma_control"), 1:4] <- c (which(names(clindata) == "diedcm2orsma_control"), "diedcm2orsma_control", "Child died and has cerebral malaria2 or severe anemaia: 0=strict control; 1=YES", "derived")
datdic[which(names(clindata) == "diedcm1notsma__control"), 1:4] <- c (which(names(clindata) == "diedcm1notsma__control"), "diedcm1notsma__control", "Child died and has cerebral malaria1 NOT severe anemaia: 0=strict control; 1=YES", "derived")
datdic[which(names(clindata) == "diedcm2notsma__control"), 1:4] <- c (which(names(clindata) == "diedcm2notsma__control"), "diedcm2notsma__control", "Child died and has cerebral malaria2 NOT severe aneamia: 0=strict control; 1=YES", "derived")
datdic[which(names(clindata) == "diedsmanotcm1_control"), 1:4] <- c(which(names(clindata) == "diedsmanotcm1_control"), "diedsmanotcm1_control", "Child died and has severe anaemia but NOT cerebral malaria1: 0=strict control; 1=YES", "derived")
datdic[which(names(clindata) == "diedsmanotcm2_control"), 1:4] <- c(which(names(clindata) == "diedsmanotcm2_control"), "diedsmanotcm2_control", "Child died and has severe anaemia but NOT cerebral malaria1: 0=strict control; 1=YES", "derived")
datdic[which(names(clindata) == "diedcm1andsma_control"), 1:4] <- c(which(names(clindata) == "diedcm1andsma_control"), "diedcm1andsma_control", "Child died and has severe anaemia AND cerebral malaria1: 0=strict control; 1=YES", "derived")
datdic[which(names(clindata) == "diedcm2andsma_control"), 1:4] <- c(which(names(clindata) == "diedcm2andsma_control"), "diedcm2andsma_control", "Child died and has severe anaemia AND cerebral malaria2: 0=strict control; 1=YES", "derived")
datdic[which(names(clindata) == "cm1_39_orsma"), 1:4] <- c(which(names(clindata) == "cm1_39_orsma"), "cm1_39_orsma", "Cerebral malaria (bcs<3 or gcs<9 and parasites) OR severe anaemia malaria (haemoglobin< or hematocrit<15 and parasites): 0=Case without trait; 1=Case with trait", "derived")
datdic[which(names(clindata) == "cm2_39_orsma"), 1:4] <- c(which(names(clindata) == "cm2_39_orsma"), "cm2_39_orsma", "Cerebral malaria2 OR severe anaemia malaria (haemoglobin< or hematocrit<15 and parasites) and NOT hypoglycemic:  0=Case without trait; 1=Case with trait", "derived")
datdic[which(names(clindata) == "cm1_39_notsma"), 1:4] <- c(which(names(clindata) == "cm1_39_notsma"), "cm1_39_notsma", "Cerebral malaria1 NOT severe anaemia malaria:  0=Case without trait; 1=Case with trait", "derived")
datdic[which(names(clindata) == "cm2_39_notsma"), 1:4] <- c(which(names(clindata) == "cm2_39_notsma"), "cm2_39_notsma", "Cerebral malaria2 NOT severe anaemia malaria: 0=Case without trait; 1=Case with trait", "derived")
datdic[which(names(clindata) == "smanotcm1_39"), 1:4] <- c(which(names(clindata) == "smanotcm1_39"), "smanotcm1_39", "Severe anaemia malaria NOT Cerebral malaria1: 0=Case without trait; 1=Case with trait", "derived")
datdic[which(names(clindata) == "smanotcm2_39"), 1:4] <- c(which(names(clindata) == "smanotcm2_39"), "smanotcm2_39", "Severe anaemia malaria NOT Cerebral malaria2: 0=Case without trait; 1=Case with trait", "derived")
datdic[which(names(clindata) == "cm1_39_andsma"), 1:4] <- c(which(names(clindata) == "cm1_39_andsma"), "cm1_39_andsma", "Severe anaemia malaria AND Cerebral malaria1: 0=Case without trait; 1=Case with trait", "derived")
datdic[which(names(clindata) == "cm2_39_andsma"), 1:4] <- c(which(names(clindata) == "cm2_39_andsma"), "cm2_39_andsma", "Severe anaemia malaria AND Cerebral malaria1: 0=Case without trait; 1=Case with trait", "derived")
datdic[which(names(clindata) == "cm1_39_orsma_control"), 1:4] <- c(which(names(clindata) == "cm1_39_orsma_control"), "cm1_39_orsma_control", "cerebral malaria1 OR severe anaemia malaria: 0=strict control; 1=Case with trait", "derived")
datdic[which(names(clindata) == "cm2_39_orsma_control"), 1:4] <- c(which(names(clindata) == "cm2_39_orsma_control"), "cm2_39_orsma_control", "cerebral malaria2 OR severe anaemia malaria: 0=strict control; 1=Case with trait", "derived")
datdic[which(names(clindata) == "cm1_39_notsma_control"), 1:4] <- c(which(names(clindata) == "cm1_39_notsma_control"), "cm1_39_notsma_control", "cerebral malaria1 NOT severe anaemia malaria: 0=strict control; 1=Case with trait", "derived")
datdic[which(names(clindata) == "cm2_39_notsma_control"), 1:4] <- c(which(names(clindata) == "cm2_39_notsma_control"), "cm2_39_notsma_control", "cerebral malaria2 NOT severe anaemia malaria: 0=strict control; 1=Case with trait", "derived")
datdic[which(names(clindata) == "smanotcm1_39_control"), 1:4] <- c(which(names(clindata) == "smanotcm1_39_control"), "smanotcm1_39_control", "Severe anaemia malaria NOT cerebral malaria1: 0=strict control; 1=Case with trait", "derived")
datdic[which(names(clindata) == "smanotcm2_39_control"), 1:4] <- c(which(names(clindata) == "smanotcm2_39_control"), "smanotcm2_39_control", "Severe anaemia malaria NOT cerebral malaria2: 0=strict control; 1=Case with trait", "derived")
datdic[which(names(clindata) == "cm1_39_andsma_control"), 1:4] <- c(which(names(clindata) == "cm1_39_andsma_control"), "cm1_39_andsma_control", "cerebral malaria1 AND severe anaemia malaria: 0=strict control; 1=Case with trait", "derived")
datdic[which(names(clindata) == "cm2_39_andsma_control"), 1:4] <- c(which(names(clindata) == "cm2_39_andsma_control"), "cm2_39_andsma_control", "cerebral malaria2 AND severe anaemia malaria: 0=strict control; 1=Case with trait", "derived")
datdic[which(names(clindata) == "casenotcm1_39notsma"), 1:4] <- c(which(names(clindata) == "casenotcm1_39notsma"), "casenotcm1_39notsma", "Strict case without cm1_39 or sma: 0=Strict control; 1=Strict case without phenotypes", "derived")
datdic[which(names(clindata) == "casenotcm1_39notsma_control"), 1:4] <- c(which(names(clindata) == "casenotcm1_39notsma_control"), "casenotcm1_39notsma_control", "Strict case without cm1_39 or sma: 0=Strict control; 1=Strict case without phenotypes", "derived")
datdic[which(names(clindata) == "curated_gender"), 1:4] <- c(which(names(clindata) == "curated_gender"),"curated_gender", "Gender M=male and F=female following curation by removing discrepencies between clin and genetic data", "derived")
datdic[which(names(clindata) == "gender_discrepencies"), 1:4] <- c(which(names(clindata) == "gender_discrepencies"), "gender_discrepencies", "Gender prior to curation: M=male; F=female; ND=conflicting result for clin and genetic data", "derived")

#add row information as column 0
#if(length(which(names(clindata) == "row")) == 0){
#  datdic <- rbind(c(0, "row", "Row number"), datdic)
#}

#drop rows with no name
names <- which(!is.na(datdic$name))
datdic <- datdic[names,]

###########################################################################################
#this bit is the final output
#main data
table(clindata$country_name)
write.table(clindata, file = clinical.out.csv ,sep=",", quote = F, row.names = F)

#need to download a cut-down version of key phenotypes and data

#data dictionary
write.table(datdic, file = clinical.out.dict ,sep=",", quote = F, row.names = F)
 
###########################################################################################
###########################################################################################

setwd("C:/Users/jshelton/Documents/CP1/CP1_remerge_2014/ToPheno_merging/All_country_merges/output_files")
datdic <- read.csv("CP1_clin_phenotypes_data_dictionary_09SEPT2014.csv", as.is = TRUE)
clindata <- read.csv("CP1_clin_phenotypes_ALL_09SEPT2014.csv", as.is = TRUE)

## Let's have a look at data availability across sites
names(datdic)
table(datdic$classification)
# resrict to cases for this
cases <- clindata[clindata$strict_caseorcontrolparents == "Case",]
# define order of countries
cases$country_name <- factor(cases$country_name, levels = c("Gambia", "Mali", "BurkinaFaso", "Ghana", "Nigeria","Cameroon", "Kenya", "Tanzania", "Malawi","VietNam", "PapuaNewGuinea"))
ctab <- table(cases$country_name)

#####
# % #
#####

# original timeless data only- %
timeless <- datdic$name[datdic$classification == "original_timeless"]
timeless <- na.omit(timeless)
timeless.df <- data.frame()
for (i in 1:length(timeless)){
  name <- timeless[i]
  tab  <- addmargins(table(cases[,name], cases$country_name))
  freq <- round((tab["Sum",1:11]/ ctab)*100, 2)
  timeless.df <- rbind(timeless.df, freq)
}
names(timeless.df) <- levels(cases$country_name)
timeless.df$variable <- timeless
timeless.df[timeless.df == 0] <- NA
write.table(timeless.df, "CP1_data_availability_cases_timeless.csv", sep = ",", row.names = F)

# original sampling data only- %
sampling <- datdic$name[datdic$classification == "original_sampling"]
sampling <- na.omit(sampling)
sampling.df <- data.frame()
for (i in 1:length(sampling)){
  name <- as.character(sampling[i])
  tab  <- addmargins(table(cases[,name], cases$country_name))
  freq <- round((tab["Sum",1:11]/ ctab)*100, 2)
  sampling.df <- rbind(sampling.df, freq)
}
names(sampling.df) <- levels(cases$country_name)
sampling.df$variable <- sampling
sampling.df[sampling.df == 0] <- NA
write.table(sampling.df, "CP1_data_availability_cases_sampling.csv", sep = ",", row.names = F)

##########
# counts #
##########

alltab <- addmargins(table(cases$country_name))

# original timeless data only- counts
timeless <- datdic$name[datdic$classification == "original_timeless"]
timeless <- na.omit(timeless)
timeless.df <- data.frame()
for (i in 1:length(timeless)){
  name <- timeless[i]
  tab  <- addmargins(table(cases[,name], cases$country_name))
  count <- tab["Sum",]
  timeless.df <- rbind(timeless.df, count)
}
names(timeless.df) <- levels(cases$country_name)
timeless.df$variable <- timeless
timeless.df[timeless.df == 0] <- NA
write.table(timeless.df, "CP1_data_availability_cases_timeless_counts.csv", sep = ",", row.names = F)


# original sampling data only- counts
sampling <- datdic$name[datdic$classification == "original_sampling"]
sampling <- na.omit(sampling)
sampling.df <- data.frame()
for (i in 1:length(sampling)){
  name <- as.character(sampling[i])
  if(length(which(is.na(cases[,name]))) == nrow(cases)) {count <- rep(NA, 12)} else {
    tab  <- addmargins(table(cases[,name], cases$country_name))
    count <- tab["Sum",]
  }
  sampling.df <- rbind(sampling.df, count)
}
names(sampling.df) <- levels(cases$country_name)
sampling.df$variable <- sampling
sampling.df[sampling.df == 0] <- NA
write.table(sampling.df, "CP1_data_availability_cases_sampling_counts.csv", sep = ",", row.names = F)
